/*
===========================================================================

This file is part of Tonberry source code.

===========================================================================
*/

#pragma once

#ifndef _PVPUTILS_H
#define _PVPUTILS_H

#include "../utils/zoneutils.h"

#include "../../common/kernel.h"
#include "../../common/showmsg.h"
#include "../../common/timer.h"
#include "../../common/utils.h"

#include "../map.h"
//#include "../message.h"
#include "../timetriggers.h"

#include "../packets/char.h"
#include "../packets/char_jobs.h"
#include "../packets/char_job_extra.h"
#include "../packets/chat_message.h"
#include "../packets/message_basic.h"
#include "../packets/message_name.h"
#include "../packets/message_special.h"
#include "../packets/message_standard.h"
#include "../packets/message_system.h"
#include "../packets/message_text.h"
#include "../packets/server_ip.h"

#include "../vana_time.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
//#include <ctime>

#include <algorithm>

enum PVPTEAM
{
	TEAM_NON = 0,
	TEAM_GREEN = 1,
	TEAM_BLUE = 2,
	TEAM_FFA = 3,
	TEAM_SANDORIA = 4,
	TEAM_BASTOK = 5,
	TEAM_WINDURST = 6
};

enum PVPSTATE
{
	STATE_PVE = 0,
	STATE_PVPDUEL = 1,
	STATE_TOURNEY1V1 = 2,
	STATE_TOURNEY4V4 = 3,
	STATE_PVPOPENWORLD = 4
};


#endif
