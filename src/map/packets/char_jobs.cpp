﻿/*
===========================================================================

  Copyright (c) 2010-2015 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#include "../../common/socket.h"

#include <string.h>

#include "char_jobs.h"
#include "../entities/charentity.h"


CCharJobsPacket::CCharJobsPacket(CCharEntity * PChar, bool resetflips)
{
	this->type = 0x1B;
	this->size = 0x32;

	ref<uint8>(0x04) = PChar->look.race;


    if (resetflips == true)
    {
        const char* Query = "UPDATE char_flips SET flip = 0 WHERE charid = %u;"; // reset the flips in SQL
        Sql_Query(SqlHandle, Query, PChar->id);

        ref<uint8>(0x08) = PChar->GetMJob();			    // подсвечиваем желтым главную профессию
        ref<uint8>(0x0B) = PChar->GetSJob();			    // подсвечиваем синим дополнительную профессию
    }

    if (resetflips == false)
    {
        uint8 flipstate = 0;
        const char* Query = "SELECT flip FROM char_flips WHERE charid = %u;";

        int32 ret = Sql_Query(SqlHandle, Query, PChar->id);

        Sql_NextRow(SqlHandle);

        if (Sql_GetUIntData(SqlHandle, 0) == 0 || Sql_GetUIntData(SqlHandle, 0) == 1 || Sql_GetUIntData(SqlHandle, 0) == 2)
        {
            flipstate = Sql_GetUIntData(SqlHandle, 0);
        }

        if (flipstate == 0) // not flipped
        {
            ref<uint8>(0x08) = PChar->GetMJob();
            ref<uint8>(0x0B) = PChar->GetSJob();
        }

        else if (flipstate == 1) // flipped
        {
            ref<uint8>(0x08) = PChar->GetSJob();
            ref<uint8>(0x0B) = PChar->GetMJob();
        }

        else if (flipstate == 2) // dw workaround
        {
            ref<uint8>(0x08) = PChar->GetMJob();
            ref<uint8>(0x0B) = (JOBTYPE)6; // thf
        }

    }

	memcpy(data+(0x0C), &PChar->jobs, 22);
    memcpy(data+(0x20), &PChar->stats,14);
	memcpy(data+(0x44), &PChar->jobs, 27);

    ref<uint32>(0x3C) = PChar->health.hp;
	ref<uint32>(0x40) = PChar->health.mp;

	ref<uint32>(0x44) = PChar->jobs.unlocked & 1;    // первый бит в unlocked отвечает за дополнительную профессию

    ref<uint16>(0x60) = PChar->m_EquipBlock;         // заблокированные ячейки экипировки
    ref<uint16>(0x62) = PChar->m_StatsDebilitation;  // битовое поле. занижение физических характеристик, характеристика становится красной и рядом появляется красная стрелка.
}
