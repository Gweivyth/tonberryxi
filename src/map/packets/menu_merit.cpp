﻿/*
===========================================================================

  Copyright (c) 2010-2015 Darkstar Dev Teams

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see http://www.gnu.org/licenses/

  This file is part of DarkStar-server source code.

===========================================================================
*/

#include "../../common/socket.h"

#include "menu_merit.h"

#include "../entities/charentity.h"
#include "../utils/charutils.h"


CMenuMeritPacket::CMenuMeritPacket(CCharEntity* PChar) 
{
	this->type = 0x63;
	this->size = 0x08;
	
	ref<uint8>(0x04) = 0x02;
	ref<uint8>(0x06) = 0x0C;

    ref<uint16>(0x08) = PChar->PMeritPoints->GetLimitPoints();
    ref<uint8>(0x0A) = PChar->PMeritPoints->GetMeritPoints();

	uint8 flag = 0x00;

	if (PChar->jobs.job[PChar->GetMJob()] >= 75 && charutils::hasKeyItem(PChar, 606))			// keyitem Limit Breaker
	{
		flag |= 0x20;
		if (PChar->MeritMode)
		{
			flag |= 0x80;
		}
	}

	//capped EXP
	if ((PChar->jobs.job[PChar->GetMJob()] >= PChar->jobs.genkai && PChar->jobs.exp[PChar->GetMJob()] == charutils::GetExpNEXTLevel(PChar->jobs.job[PChar->GetMJob()]) - 1) 
		|| PChar->MeritMode)
	{
		flag |= 0x40;
	}

	ref<uint8>(0x0B) = flag;
    ref<uint8>(0x0C) = map_config.max_merit_points + PChar->PMeritPoints->GetMeritValue(MERIT_MAX_MERIT, PChar);

    PChar->pushPacket(new CBasicPacket(*this));

    // ver 30121205_4 second packet

    this->size = 0x6E;

	memset(data + 4, 0, sizeof(PACKET_SIZE -4));

    uint8 packet[] = 
    {
		0x03, 0x00, 0xD8
    };

	memcpy(data+(0x04), &packet, sizeof(packet));
	PChar->pushPacket(new CBasicPacket(*this));
    /*
    // mystery packet ?!?!?!?

    this->size = 0x4E;

    memset(data + 4, 0, sizeof(PACKET_SIZE - 4));

    ref<uint8>(0x04) = 0x05;
    ref<uint8>(0x06) = 0x98;
    ref<uint8>(0x08) = 0x01;
    ref<uint8>(0x09) = 0x08;
    ref<uint8>(0x0A) = 0x0A;
    ref<uint8>(0x0B) = 0x0A;

    ref<uint8>(0x1E) = 0xF2;
    ref<uint8>(0x1F) = 0x43;

    ref<uint8>(0x2A) = 0xB2;
    ref<uint8>(0x2B) = 0x43;
    ref<uint8>(0x2C) = 0x01;
    ref<uint8>(0x2E) = 0x03;

    ref<uint8>(0x4E) = 0xED;
    ref<uint8>(0x4F) = 0x19;

    ref<uint8>(0x50) = 0x02;

    ref<uint8>(0x66) = 0x20;
    ref<uint8>(0x67) = 0x35;

    ref<uint8>(0x7E) = 0x95;
    ref<uint8>(0x7F) = 0x4C;

    ref<uint8>(0x03) = 0x03;
    ref<uint8>(0x01) = 0x01;
    ref<uint8>(0x2E) = 0x2E;
    ref<uint8>(0x5A) = 0x5A; /// no effect

    PChar->pushPacket(new CBasicPacket(*this));

    // mystery packet #2

    this->size = 0x46;

    memset(data + 4, 0, sizeof(PACKET_SIZE - 4));

    ref<uint8>(0x04) = 0x07;
    ref<uint8>(0x06) = 0x88;
    ref<uint8>(0x0A) = 0xFF;
    ref<uint8>(0x0B) = 0xFF;
    ref<uint8>(0x0C) = 0x80;
    ref<uint8>(0x0D) = 0x1E;
    ref<uint8>(0x0E) = 0x24;
    ref<uint8>(0x0F) = 0x1F;

    ref<uint8>(0x10) = 0x09; /// no effect

    PChar->pushPacket(new CBasicPacket(*this));

    // mystery packet #3

    this->size = 0x46;

    memset(data + 4, 0, sizeof(PACKET_SIZE - 4));

    ref<uint8>(0x04) = 0x07;
    ref<uint8>(0x06) = 0x88;
    ref<uint8>(0x09) = 0x01;
    ref<uint8>(0x0A) = 0x13;
    ref<uint8>(0x0B) = 0x20;

    ref<uint8>(0x10) = 0x59;
    ref<uint8>(0x14) = 0x42;
    ref<uint8>(0x18) = 0x3D;
    ref<uint8>(0x1C) = 0xCA;
    ref<uint8>(0x0D) = 0x01;

    ref<uint8>(0x20) = 0x37;
    ref<uint8>(0x24) = 0x3E;
    ref<uint8>(0x28) = 0x3E;
    ref<uint8>(0x2C) = 0x44;

    ref<uint8>(0x30) = 0x28;
    ref<uint8>(0x34) = 0x6D;
    ref<uint8>(0x35) = 0x03;
    ref<uint8>(0x38) = 0xCC; /// no effect

    PChar->pushPacket(new CBasicPacket(*this));

    // mystery packet #4

    this->size = 0x46;

    memset(data + 4, 0, sizeof(PACKET_SIZE - 4));

    ref<uint8>(0x04) = 0x07;
    ref<uint8>(0x06) = 0x88;
    ref<uint8>(0x09) = 0x03;
    ref<uint8>(0x0A) = 0xFF;
    ref<uint8>(0x0B) = 0xFF; /// no effect

    PChar->pushPacket(new CBasicPacket(*this));

    // mystery packet #5

    this->size = 0x46;

    memset(data + 4, 0, sizeof(PACKET_SIZE - 4));

    ref<uint8>(0x04) = 0x07;
    ref<uint8>(0x06) = 0x88;
    ref<uint8>(0x08) = 0x01;
    ref<uint8>(0x09) = 0x02;

    ref<uint8>(0x10) = 0x19;
    ref<uint8>(0x11) = 0x05;
    ref<uint8>(0x14) = 0xA4;
    ref<uint8>(0x15) = 0x01;
    ref<uint8>(0x18) = 0x66;
    ref<uint8>(0x19) = 0x03;
    ref<uint8>(0x1C) = 0xFA;
    ref<uint8>(0x1D) = 0xBE;

    ref<uint8>(0x20) = 0xF6;
    ref<uint8>(0x21) = 0x02;
    ref<uint8>(0x24) = 0x0D;
    ref<uint8>(0x25) = 0x07;
    ref<uint8>(0x28) = 0xAB;
    ref<uint8>(0x29) = 0x01;
    ref<uint8>(0x2C) = 0x89;
    ref<uint8>(0x2D) = 0x04;

    ref<uint8>(0x30) = 0xF9;
    ref<uint8>(0x34) = 0xF7;
    ref<uint8>(0x35) = 0x39;
    ref<uint8>(0x36) = 0x01;
    ref<uint8>(0x38) = 0xD5;
    ref<uint8>(0x39) = 0xA9; /// no effect

    PChar->pushPacket(new CBasicPacket(*this));

    // msgbasic upgrade packet

    this->type = 0x29;
    this->size = 0x0E;

    memset(data + 4, 0, sizeof(PACKET_SIZE - 4));

    ref<uint8>(0x04) = 0xF8;
    ref<uint8>(0x05) = 0x74;
    ref<uint8>(0x06) = 0x04;
    ref<uint8>(0x07) = 0x00;
    ref<uint32>(0x08) = PChar->id;
    ref<uint8>(0x0C) = 0xE3;
    ref<uint8>(0x0D) = 0x05;
    ref<uint8>(0x0E) = 0x00;
    ref<uint8>(0x0F) = 0x00;

    ref<uint8>(0x10) = 0x01;

    ref<uint8>(0x14) = 0x5E;
    ref<uint8>(0x15) = 0x04;
    ref<uint8>(0x16) = 0x5E;
    ref<uint8>(0x17) = 0x04;
    ref<uint8>(0x18) = 0x7C;
    ref<uint8>(0x19) = 0x01; /// no effect, didnt evne sent a msg

    PChar->pushPacket(new CBasicPacket(*this));
    */
	// ver 30130319_5 third packet

	this->size = 0x44;

	memset(data + 4, 0, sizeof(PACKET_SIZE -4));

	uint8 packet2[] = 
	{
		0x04, 0x00, 0x84
	};
	memcpy(data+(0x04), &packet2, sizeof(packet2));
}




//0x63, 0x06, 0x88, 0x41, 0x02, 0x00, 0x08, 0x00, 0xD3, 0x03, 0x03, 0x60