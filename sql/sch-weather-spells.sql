-- 4104	fire_cluster -- 6049 -- Firestorm Schema
-- 4105	ice_cluster -- 6052 -- Hailstorm Schema
-- 4106	wind_cluster -- 6054 -- Windstorm Schema
-- 4107	earth_cluster -- 6053 -- Sandstorm Schema
-- 4108	lightning_cluster -- 6051 -- Thunderstorm Schema
-- 4109	water_cluster -- 6050 -- Rainstorm Schema
-- 4110	light_cluster -- 6055 -- Aurorastorm Schema
-- 4111	dark_cluster -- 6056 -- Voidstorm Schema

-- 6047  -- Luminohelix Schema
-- 6048  -- Noctohelix Schema


DELETE FROM dspdb.mob_droplist WHERE itemId IN (6049,6052,6054,6053,6051,6050,6055,6056); -- weather spells
DELETE FROM dspdb.mob_droplist WHERE itemId IN (6047,6048); -- light/dark helix spells

INSERT INTO dspdb.mob_droplist VALUES (41,0,0,1000,6054,30); -- wind
INSERT INTO dspdb.mob_droplist VALUES (42,0,0,1000,6054,30);
INSERT INTO dspdb.mob_droplist VALUES (43,0,0,1000,6054,30);

INSERT INTO dspdb.mob_droplist VALUES (639,0,0,1000,6056,10); -- dark
INSERT INTO dspdb.mob_droplist VALUES (640,0,0,1000,6056,10);
INSERT INTO dspdb.mob_droplist VALUES (641,0,0,1000,6056,10);
INSERT INTO dspdb.mob_droplist VALUES (642,0,0,1000,6056,10);

INSERT INTO dspdb.mob_droplist VALUES (639,0,0,1000,6048,10); -- dark helix
INSERT INTO dspdb.mob_droplist VALUES (640,0,0,1000,6048,10);
INSERT INTO dspdb.mob_droplist VALUES (641,0,0,1000,6048,10);
INSERT INTO dspdb.mob_droplist VALUES (642,0,0,1000,6048,10);

INSERT INTO dspdb.mob_droplist VALUES (829,0,0,1000,6053,22); -- earth
INSERT INTO dspdb.mob_droplist VALUES (830,0,0,1000,6053,22);
INSERT INTO dspdb.mob_droplist VALUES (831,0,0,1000,6053,22);
INSERT INTO dspdb.mob_droplist VALUES (832,0,0,1000,6053,22);
INSERT INTO dspdb.mob_droplist VALUES (833,0,0,1000,6053,22);

INSERT INTO dspdb.mob_droplist VALUES (955,0,0,1000,6049,27); -- fire
INSERT INTO dspdb.mob_droplist VALUES (956,0,0,1000,6049,27);
INSERT INTO dspdb.mob_droplist VALUES (957,0,0,1000,6049,27);
INSERT INTO dspdb.mob_droplist VALUES (958,0,0,1000,6049,27);
INSERT INTO dspdb.mob_droplist VALUES (959,0,0,1000,6049,27);

INSERT INTO dspdb.mob_droplist VALUES (1754,0,0,1000,6052,20); -- ice
INSERT INTO dspdb.mob_droplist VALUES (1755,0,0,1000,6052,20);
INSERT INTO dspdb.mob_droplist VALUES (1756,0,0,1000,6052,20);
INSERT INTO dspdb.mob_droplist VALUES (1757,0,0,1000,6052,20);
INSERT INTO dspdb.mob_droplist VALUES (1758,0,0,1000,6052,20);
INSERT INTO dspdb.mob_droplist VALUES (1759,0,0,1000,6052,20);

INSERT INTO dspdb.mob_droplist VALUES (2059,0,0,1000,6055,23); -- light

INSERT INTO dspdb.mob_droplist VALUES (2059,0,0,1000,6047,23); -- light helix

INSERT INTO dspdb.mob_droplist VALUES (3481,0,0,1000,6051,10); -- lightning
INSERT INTO dspdb.mob_droplist VALUES (3482,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3483,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3484,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3485,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3486,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3487,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3488,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3489,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3490,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3491,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3492,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3493,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3494,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3495,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3496,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3497,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3498,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3499,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3500,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3501,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3502,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3503,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3504,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3505,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3506,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3507,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3508,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3509,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3510,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3511,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3512,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3513,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3514,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3515,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3516,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3517,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3518,0,0,1000,6051,10);
INSERT INTO dspdb.mob_droplist VALUES (3519,0,0,1000,6051,10);

INSERT INTO dspdb.mob_droplist VALUES (4087,0,0,1000,6050,40); -- water
INSERT INTO dspdb.mob_droplist VALUES (4088,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4089,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4090,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4091,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4092,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4093,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4094,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4095,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4096,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4097,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4098,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4099,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4100,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4101,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4102,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4103,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4104,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4105,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4106,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4107,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4108,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4109,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4110,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4111,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4112,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4113,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4114,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4115,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4116,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4117,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4118,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4119,0,0,1000,6050,40);
INSERT INTO dspdb.mob_droplist VALUES (4120,0,0,1000,6050,40);



SELECT * FROM dspdb.mob_droplist WHERE (itemId > 4103 and itemId < 4112 and itemRate in (150,400,1000)) OR (itemId in (6049,6052,6054,6053,6051,6050,6055,6056,6047,6048));