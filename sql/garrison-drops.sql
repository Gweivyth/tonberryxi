
-- DELETE FROM dspdb.bcnm_loot WHERE itemid IN ;


/*

LEVEL 20:
13818	garrison_tunica
14206	garrison_boots
14314	garrison_hose
14841	garrison_gloves
15147	garrison_sallet

LEVEL 30:
17272	military_gun
17580	military_pole
17839	military_harp
17940	military_pick
18090	military_spear
18212	military_axe

LEVEL 40:
N/A

LEVEL 50:
14756	accurate_earring
14744	undead_earring
14745	arcana_earring
14747	bird_earring
14748	amorph_earring
14749	lizard_earring
14750	aquan_earring
14752	beast_earring
14753	demon_earring
14754	dragon_earring
14746	vermin_earring
14751	plantoid_earring

LEVEL 75:
17204	mighty_bow
17465	mighty_cudgel
17581	mighty_pole
17697	mighty_talwar
17941	mighty_pick
18000	mighty_knife
18049	mighty_zaghnal
18091	mighty_lance
18213	mighty_axe
18352	mighty_patas
18374	mighty_sword
17791	rai_kunimitsu
17824	nukemaru

*/

/*
34  wings_of_fury				BCNM20		3  members
105 charming_trio				BCNM20		3  members

35  petrifying_pair				BCNM30		3  members
104 creeping doom				BCNM30		3  members
10  dropping_like_flies			BCNM30		6  members

12  under_observation			BCNM40		3  members

 4  hostile_herbivores 			BCNM50		6  members
 
 79  up_in_arms					BCNM60		3  members
129 jungle_boogymen				BCNM60		6  members
130 amphibian_assault			BCNM60		6  members

15  double_dragonian			KSNM30		6  members
17  contaminated_colosseum		KSNM30		6  members
81  operation_desert_swarm		KSNM30		6  members
82  prehistoric_pigeons			KSNM30		6  members

11  horns_of_war 				KSNM99		18 members
76  hills_are_alive				KSNM99		18 members
107 early_bird_catches_the_wyrm	KSNM99		18 members

*/

-- LootDropId, ItemId, rate, group

INSERT INTO dspdb.bcnm_loot VALUES (34,13818,90,6); -- WoF
INSERT INTO dspdb.bcnm_loot VALUES (34,14206,90,6);
INSERT INTO dspdb.bcnm_loot VALUES (34,14314,90,6); -- 45% chance to drop level 20 gear
INSERT INTO dspdb.bcnm_loot VALUES (34,14841,90,6);
INSERT INTO dspdb.bcnm_loot VALUES (34,15147,90,6);

INSERT INTO dspdb.bcnm_loot VALUES (105,13818,90,8); -- CT
INSERT INTO dspdb.bcnm_loot VALUES (105,14206,90,8);
INSERT INTO dspdb.bcnm_loot VALUES (105,14314,90,8); -- 45% chance to drop level 20 gear
INSERT INTO dspdb.bcnm_loot VALUES (105,14841,90,8);
INSERT INTO dspdb.bcnm_loot VALUES (105,15147,90,8);


INSERT INTO dspdb.bcnm_loot VALUES (35,17272,60,9); -- PP
INSERT INTO dspdb.bcnm_loot VALUES (35,17580,60,9);
INSERT INTO dspdb.bcnm_loot VALUES (35,17839,60,9); -- 36% chance to drop level 30 gear
INSERT INTO dspdb.bcnm_loot VALUES (35,17940,60,9);
INSERT INTO dspdb.bcnm_loot VALUES (35,18090,60,9);
INSERT INTO dspdb.bcnm_loot VALUES (35,18212,60,9);

INSERT INTO dspdb.bcnm_loot VALUES (104,17272,60,10); -- CD
INSERT INTO dspdb.bcnm_loot VALUES (104,17580,60,10);
INSERT INTO dspdb.bcnm_loot VALUES (104,17839,60,10); -- 36% chance to drop level 30 gear
INSERT INTO dspdb.bcnm_loot VALUES (104,17940,60,10);
INSERT INTO dspdb.bcnm_loot VALUES (104,18090,60,10);
INSERT INTO dspdb.bcnm_loot VALUES (104,18212,60,10);

INSERT INTO dspdb.bcnm_loot VALUES (12,17272,150,7); -- DLF
INSERT INTO dspdb.bcnm_loot VALUES (12,17580,150,7);
INSERT INTO dspdb.bcnm_loot VALUES (12,17839,150,7); -- 90% chance to drop level 30 gear
INSERT INTO dspdb.bcnm_loot VALUES (12,17940,150,7);
INSERT INTO dspdb.bcnm_loot VALUES (12,18090,150,7);
INSERT INTO dspdb.bcnm_loot VALUES (12,18212,150,7);

INSERT INTO dspdb.bcnm_loot VALUES (16,17272,20,7); -- UO
INSERT INTO dspdb.bcnm_loot VALUES (16,17580,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,17839,20,7); -- 36% chance to drop level 30 or level 50 gear
INSERT INTO dspdb.bcnm_loot VALUES (16,17940,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,18090,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,18212,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14756,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14744,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14745,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14747,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14748,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14749,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14750,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14752,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14753,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14754,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14746,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (16,14751,20,7);

INSERT INTO dspdb.bcnm_loot VALUES (14,14756,80,5); -- HH
INSERT INTO dspdb.bcnm_loot VALUES (14,14744,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14745,80,5); -- 96% chance to drop level 50 gear (it's mostly garbage)
INSERT INTO dspdb.bcnm_loot VALUES (14,14747,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14748,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14749,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14750,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14752,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14753,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14754,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14746,80,5);
INSERT INTO dspdb.bcnm_loot VALUES (14,14751,80,5);

INSERT INTO dspdb.bcnm_loot VALUES (79,17204,30,8); -- UIA
INSERT INTO dspdb.bcnm_loot VALUES (79,17465,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,17581,30,8); -- 39% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (79,17697,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,17941,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,18000,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,18049,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,18091,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,18213,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,18352,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,18374,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,17791,30,8);
INSERT INTO dspdb.bcnm_loot VALUES (79,17824,30,8);

INSERT INTO dspdb.bcnm_loot VALUES (10,17204,70,7); -- JB
INSERT INTO dspdb.bcnm_loot VALUES (10,17465,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,17581,70,7); -- 91% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (10,17697,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,17941,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,18000,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,18049,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,18091,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,18213,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,18352,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,18374,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,17791,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (10,17824,70,7);


INSERT INTO dspdb.bcnm_loot VALUES (156,17204,70,7); -- AA
INSERT INTO dspdb.bcnm_loot VALUES (156,17465,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,17581,70,7); -- 91% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (156,17697,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,17941,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,18000,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,18049,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,18091,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,18213,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,18352,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,18374,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,17791,70,7);
INSERT INTO dspdb.bcnm_loot VALUES (156,17824,70,7);

INSERT INTO dspdb.bcnm_loot VALUES (15,17204,50,8); -- DD
INSERT INTO dspdb.bcnm_loot VALUES (15,17465,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,17581,50,8); -- 65% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (15,17697,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,17941,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,18000,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,18049,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,18091,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,18213,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,18352,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,18374,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,17791,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (15,17824,50,8);

INSERT INTO dspdb.bcnm_loot VALUES (17,17204,50,8); -- CC
INSERT INTO dspdb.bcnm_loot VALUES (17,17465,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,17581,50,8); -- 65% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (17,17697,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,17941,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,18000,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,18049,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,18091,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,18213,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,18352,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,18374,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,17791,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (17,17824,50,8);

INSERT INTO dspdb.bcnm_loot VALUES (81,17204,50,7); -- ODS
INSERT INTO dspdb.bcnm_loot VALUES (81,17465,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,17581,50,7); -- 65% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (81,17697,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,17941,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,18000,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,18049,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,18091,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,18213,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,18352,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,18374,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,17791,50,7);
INSERT INTO dspdb.bcnm_loot VALUES (81,17824,50,7);

INSERT INTO dspdb.bcnm_loot VALUES (82,17204,50,8); -- PHP
INSERT INTO dspdb.bcnm_loot VALUES (82,17465,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,17581,50,8); -- 65% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (82,17697,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,17941,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,18000,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,18049,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,18091,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,18213,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,18352,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,18374,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,17791,50,8);
INSERT INTO dspdb.bcnm_loot VALUES (82,17824,50,8);

INSERT INTO dspdb.bcnm_loot VALUES (11,17204,70,12); -- HOW
INSERT INTO dspdb.bcnm_loot VALUES (11,17465,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,17581,70,12); -- 91% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (11,17697,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,17941,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,18000,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,18049,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,18091,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,18213,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,18352,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,18374,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,17791,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (11,17824,70,12);

INSERT INTO dspdb.bcnm_loot VALUES (76,17204,70,12); -- THAL
INSERT INTO dspdb.bcnm_loot VALUES (76,17465,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,17581,70,12); -- 91% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (76,17697,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,17941,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,18000,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,18049,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,18091,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,18213,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,18352,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,18374,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,17791,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (76,17824,70,12);

INSERT INTO dspdb.bcnm_loot VALUES (107,17204,70,12); -- EBCTW
INSERT INTO dspdb.bcnm_loot VALUES (107,17465,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,17581,70,12); -- 91% chance to drop level 75 gear
INSERT INTO dspdb.bcnm_loot VALUES (107,17697,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,17941,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,18000,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,18049,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,18091,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,18213,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,18352,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,18374,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,17791,70,12);
INSERT INTO dspdb.bcnm_loot VALUES (107,17824,70,12);





SELECT * FROM dspdb.bcnm_loot WHERE itemid IN (13818,
14206,
14314,
14841,
15147,
17272,
17580,
17839,
17940,
18090,
18212,
14756,
14744,
14745,
14747,
14748,
14749,
14750,
14752,
14753,
14754,
14746,
14751,
17204,
17465,
17581,
17697,
17941,
18000,
18049,
18091,
18213,
18352,
18374,
17791,
17824);