-- jugner82 -> vunkerl83 = -393x
-- vunkerl83 -> jugner82 = 621x rot 149 on arrival
UPDATE dspdb.zonelines SET tozone = 82, tox = 621.865, toy = -6.665, toz = 300.264, rotation = 149 WHERE zoneline = 878785146;

-- jugner82 -> batallia84 = -439x rot 253
-- batallia84 -> jugner82 = 584x rot 125
-- want: 584.034 -8.476	558.296	125
UPDATE dspdb.zonelines SET tozone = 82, tox = 584.034, toy = -8.476, toz = 558.296, rotation = 125 WHERE zoneline = 845230714;

-- jugner to vunkerl -579 rot 196
-- vunkerl to jugner 541 rot 98
-- want 82	541.857	-6.664	-140.268	98
UPDATE dspdb.zonelines SET tozone = 82, tox = 541.857, toy = -6.664, toz = -140.268, rotation = 98 WHERE zoneline = 912339578;

-- jugner to vunkerl -661 rot 56
-- vunk to jug is 180 rot 220
-- want 82 180.335 22.653 -581.766 220

UPDATE dspdb.zonelines SET tozone = 82, tox = 180.335, toy = 22.653, toz = -581.766, rotation = 220 WHERE zoneline = 945894010;

-- ronf and jug... also debug for La Vaule, wtf is that??

UPDATE dspdb.zonelines SET tozone = 82, tox = -339.471, toy = -6.782, toz = 462.074, rotation = 35 WHERE zoneline in (811676282,1633759866);

SELECT * FROM dspdb.zonelines WHERE fromzone = 82 or tozone = 82 ORDER BY fromzone ASC;