-- replace Pugil with your mob's name
-- replace the zoneip with your map server's IP address

USE dspdb;

SELECT mob_groups.groupid,mob_groups.poolid,mob_groups.respawntime,mob_groups.spawntype,mob_groups.dropid,mob_groups.HP,mob_groups.MP,mob_groups.minLevel,mob_groups.maxLevel,mob_groups.allegiance,
mob_groups.zoneid,zone_settings.name FROM mob_groups
INNER JOIN zone_settings ON mob_groups.zoneid = zone_settings.zoneid
WHERE mob_groups.groupid IN
(SELECT mob_spawn_points.groupid FROM mob_spawn_points WHERE mob_spawn_points.mobname LIKE 'Pugil' AND (pos_x NOT IN (0.000) AND pos_y NOT IN (0.000) AND pos_z NOT IN (0.000)))
AND mob_groups.zoneid IN (SELECT zone_settings.zoneid FROM zone_settings WHERE zoneip IN ('127.0.0.1'));
