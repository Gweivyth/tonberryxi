DELETE FROM dspdb.bcnm_loot WHERE itemId IN (1255,1256,1257,1258,1259,1260,1261,1262);

-- LootDropId, ItemId, rate, group

INSERT INTO dspdb.bcnm_loot VALUES (14,1255,25,4); -- HH
INSERT INTO dspdb.bcnm_loot VALUES (14,1256,25,4);
INSERT INTO dspdb.bcnm_loot VALUES (14,1257,25,4);
INSERT INTO dspdb.bcnm_loot VALUES (14,1258,25,4);
INSERT INTO dspdb.bcnm_loot VALUES (14,1259,25,4);
INSERT INTO dspdb.bcnm_loot VALUES (14,1260,25,4);
INSERT INTO dspdb.bcnm_loot VALUES (14,1261,20,4);
INSERT INTO dspdb.bcnm_loot VALUES (14,1262,20,4);

INSERT INTO dspdb.bcnm_loot VALUES (12,1255,15,6); -- DLF
INSERT INTO dspdb.bcnm_loot VALUES (12,1256,15,6);
INSERT INTO dspdb.bcnm_loot VALUES (12,1257,15,6);
INSERT INTO dspdb.bcnm_loot VALUES (12,1258,15,6);
INSERT INTO dspdb.bcnm_loot VALUES (12,1259,15,6);
INSERT INTO dspdb.bcnm_loot VALUES (12,1260,15,6);
INSERT INTO dspdb.bcnm_loot VALUES (12,1261,12,6);
INSERT INTO dspdb.bcnm_loot VALUES (12,1262,12,6);

INSERT INTO dspdb.bcnm_loot VALUES (11,1255,100,11); -- HOW
INSERT INTO dspdb.bcnm_loot VALUES (11,1256,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (11,1257,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (11,1258,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (11,1259,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (11,1260,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (11,1261,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (11,1262,100,11);

INSERT INTO dspdb.bcnm_loot VALUES (16,1255,20,6); -- UO
INSERT INTO dspdb.bcnm_loot VALUES (16,1256,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (16,1257,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (16,1258,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (16,1259,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (16,1260,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (16,1261,10,6);
INSERT INTO dspdb.bcnm_loot VALUES (16,1262,10,6);

INSERT INTO dspdb.bcnm_loot VALUES (15,1255,20,7); -- DD
INSERT INTO dspdb.bcnm_loot VALUES (15,1256,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (15,1257,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (15,1258,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (15,1259,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (15,1260,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (15,1261,16,7);
INSERT INTO dspdb.bcnm_loot VALUES (15,1262,16,7);

INSERT INTO dspdb.bcnm_loot VALUES (17,1255,20,7); -- CC
INSERT INTO dspdb.bcnm_loot VALUES (17,1256,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (17,1257,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (17,1258,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (17,1259,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (17,1260,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (17,1261,16,7);
INSERT INTO dspdb.bcnm_loot VALUES (17,1262,16,7);

INSERT INTO dspdb.bcnm_loot VALUES (34,1255,6,5); -- WoF
INSERT INTO dspdb.bcnm_loot VALUES (34,1256,6,5);
INSERT INTO dspdb.bcnm_loot VALUES (34,1257,6,5);
INSERT INTO dspdb.bcnm_loot VALUES (34,1258,6,5);
INSERT INTO dspdb.bcnm_loot VALUES (34,1259,6,5);
INSERT INTO dspdb.bcnm_loot VALUES (34,1260,6,5);
INSERT INTO dspdb.bcnm_loot VALUES (34,1261,3,5);
INSERT INTO dspdb.bcnm_loot VALUES (34,1262,3,5);

INSERT INTO dspdb.bcnm_loot VALUES (35,1255,13,8); -- PP
INSERT INTO dspdb.bcnm_loot VALUES (35,1256,13,8);
INSERT INTO dspdb.bcnm_loot VALUES (35,1257,13,8);
INSERT INTO dspdb.bcnm_loot VALUES (35,1258,13,8);
INSERT INTO dspdb.bcnm_loot VALUES (35,1259,13,8);
INSERT INTO dspdb.bcnm_loot VALUES (35,1260,13,8);
INSERT INTO dspdb.bcnm_loot VALUES (35,1261,5,8);
INSERT INTO dspdb.bcnm_loot VALUES (35,1262,5,8);

INSERT INTO dspdb.bcnm_loot VALUES (76,1255,100,11); -- THAL
INSERT INTO dspdb.bcnm_loot VALUES (76,1256,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (76,1257,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (76,1258,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (76,1259,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (76,1260,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (76,1261,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (76,1262,100,11);

INSERT INTO dspdb.bcnm_loot VALUES (79,1255,25,7); -- UIA
INSERT INTO dspdb.bcnm_loot VALUES (79,1256,25,7);
INSERT INTO dspdb.bcnm_loot VALUES (79,1257,25,7);
INSERT INTO dspdb.bcnm_loot VALUES (79,1258,25,7);
INSERT INTO dspdb.bcnm_loot VALUES (79,1259,25,7);
INSERT INTO dspdb.bcnm_loot VALUES (79,1260,25,7);
INSERT INTO dspdb.bcnm_loot VALUES (79,1261,15,7);
INSERT INTO dspdb.bcnm_loot VALUES (79,1262,15,7);

INSERT INTO dspdb.bcnm_loot VALUES (81,1255,20,6); -- ODS
INSERT INTO dspdb.bcnm_loot VALUES (81,1256,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (81,1257,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (81,1258,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (81,1259,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (81,1260,20,6);
INSERT INTO dspdb.bcnm_loot VALUES (81,1261,16,6);
INSERT INTO dspdb.bcnm_loot VALUES (81,1262,16,6);

INSERT INTO dspdb.bcnm_loot VALUES (82,1255,20,7); -- PHP
INSERT INTO dspdb.bcnm_loot VALUES (82,1256,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (82,1257,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (82,1258,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (82,1259,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (82,1260,20,7);
INSERT INTO dspdb.bcnm_loot VALUES (82,1261,16,7);
INSERT INTO dspdb.bcnm_loot VALUES (82,1262,16,7);

INSERT INTO dspdb.bcnm_loot VALUES (104,1255,13,9); -- CD
INSERT INTO dspdb.bcnm_loot VALUES (104,1256,13,9);
INSERT INTO dspdb.bcnm_loot VALUES (104,1257,13,9);
INSERT INTO dspdb.bcnm_loot VALUES (104,1258,13,9);
INSERT INTO dspdb.bcnm_loot VALUES (104,1259,13,9);
INSERT INTO dspdb.bcnm_loot VALUES (104,1260,13,9);
INSERT INTO dspdb.bcnm_loot VALUES (104,1261,5,9);
INSERT INTO dspdb.bcnm_loot VALUES (104,1262,5,9);

INSERT INTO dspdb.bcnm_loot VALUES (105,1255,6,7); -- CT
INSERT INTO dspdb.bcnm_loot VALUES (105,1256,6,7);
INSERT INTO dspdb.bcnm_loot VALUES (105,1257,6,7);
INSERT INTO dspdb.bcnm_loot VALUES (105,1258,6,7);
INSERT INTO dspdb.bcnm_loot VALUES (105,1259,6,7);
INSERT INTO dspdb.bcnm_loot VALUES (105,1260,6,7);
INSERT INTO dspdb.bcnm_loot VALUES (105,1261,3,7);
INSERT INTO dspdb.bcnm_loot VALUES (105,1262,3,7);

INSERT INTO dspdb.bcnm_loot VALUES (107,1255,100,11); -- EBCTW
INSERT INTO dspdb.bcnm_loot VALUES (107,1256,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (107,1257,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (107,1258,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (107,1259,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (107,1260,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (107,1261,100,11);
INSERT INTO dspdb.bcnm_loot VALUES (107,1262,100,11);

INSERT INTO dspdb.bcnm_loot VALUES (10,1255,34,6); -- JB
INSERT INTO dspdb.bcnm_loot VALUES (10,1256,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (10,1257,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (10,1258,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (10,1259,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (10,1260,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (10,1261,30,6);
INSERT INTO dspdb.bcnm_loot VALUES (10,1262,30,6);

INSERT INTO dspdb.bcnm_loot VALUES (156,1255,34,6); -- AA
INSERT INTO dspdb.bcnm_loot VALUES (156,1256,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (156,1257,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (156,1258,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (156,1259,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (156,1260,34,6);
INSERT INTO dspdb.bcnm_loot VALUES (156,1261,30,6);
INSERT INTO dspdb.bcnm_loot VALUES (156,1262,30,6);

SELECT * FROM dspdb.bcnm_loot WHERE itemid IN (1255,1256,1257,1258,1259,1260,1261,1262);