-- inner horutoro 192
-- UPDATE dspdb.mob_groups SET minLevel = 17 WHERE groupid in (10797,10798,10804); -- troika bats, deathwatch beetle and covin bat
-- UPDATE dspdb.mob_groups SET maxLevel = 20 WHERE groupid in (10797,10798,10804); -- troika bats, deathwatch beetle and covin bat
-- UPDATE dspdb.mob_groups SET minLevel = 19 WHERE groupid in (10799,10800,10801,10802,10803,10805);
-- UPDATE dspdb.mob_groups SET maxLevel = 22 WHERE groupid in (10799,10800,10801,10802,10803,10805);

-- outer horutoro 194
-- UPDATE dspdb.mob_groups SET minLevel = 13, maxLevel = 16 WHERE groupid in (10807); -- slimes
-- UPDATE dspdb.mob_groups SET minLevel = 14, maxLevel = 17 WHERE groupid in (10806,10808); -- bats

-- dangruf 191
-- UPDATE dspdb.mob_groups SET minLevel = 23, maxLevel = 25 WHERE groupid in (10779); -- opo
-- UPDATE dspdb.mob_groups SET minLevel = 21, maxLevel = 23 WHERE groupid in (10773,10774,10775,10782,10783,10784,10777); -- gobs and leeches
-- UPDATE dspdb.mob_groups SET minLevel = 16, maxLevel = 20 WHERE groupid in (10781); -- lizards
-- UPDATE dspdb.mob_groups SET minLevel = 15, maxLevel = 18 WHERE groupid in (10780,10778,10776); -- rabbits, worms, crabs

-- CN 197
-- UPDATE dspdb.mob_groups SET minLevel = 50, maxLevel = 53 WHERE groupid = 10767; -- dancing jewel
-- UPDATE dspdb.mob_groups SET minLevel = 51, maxLevel = 54 WHERE groupid = 10768; -- olid funguar
-- UPDATE dspdb.mob_groups SET minLevel = 53, maxLevel = 55 WHERE groupid = 10765; -- king crawler
-- UPDATE dspdb.mob_groups SET minLevel = 55, maxLevel = 57 WHERE groupid = 10766; -- vespo

-- tree 153
-- UPDATE dspdb.mob_groups SET minLevel = 80, maxLevel = 82 WHERE zoneid = 153 AND minLevel > 80 AND respawntime > 0; -- getting lazy

-- SELECT * FROM dspdb.mob_groups WHERE zoneid in (153) and respawntime > 0 and minLevel > 1 ORDER BY minLevel ASC;


USE dspdb;

update `mob_spawn_points` set `groupid` = replace(`groupid`,'10744','8146'); -- Changed Snaggletooth Peapuks to Darters for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10745','8160'); -- Changed Mourning Crawlers to Processionaire for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10746','9039'); -- Changed Kuftal Delvers to Kuftal Diggers for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10747','9042'); -- Changed Machairoduses to Ovinnik for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10748','8981'); -- Changed Colliery Bats to Ding Bats for 75 content
update `mob_groups` set `minLevel` = '3' where `groupid` = 10749; -- Changed Burrower Worm min level for 75 content
update `mob_groups` set `maxLevel` = '5' where `groupid` = 10749; -- Changed Burrower Worm min level for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10750','8986'); -- Changed Soot Crabs to River Crab for 75 content SKIPPED
update `mob_groups` set `minLevel` = '3' where `groupid` = 10751; -- Changed Veindigger Leech min level for 75 content
update `mob_groups` set `maxLevel` = '5' where `groupid` = 10751; -- Changed Veindigger Leech min level for 75 content
delete from `mob_spawn_points` where `groupid` = 10752; -- Removed Wurdalaks (Vampyrs) for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10753','8786'); -- Changed Blind Bats to Werebats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10754','8782'); -- Changed Panna Cotta to Mousse for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10755','8779'); -- Changed Nachtmahrs to Haunt for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10756','8777'); -- Changed Dabilla to Garm for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10757','9949'); -- Changed Wekufes to Revenants for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10758','9932'); -- Changed Sentient Carafes to Clockwork Pods for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10759','9959'); -- Changed Balayang to Vampire Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10760','2291'); -- Changed Boulder Eaters to Mold Eaters for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10761','8164'); -- Changed Pygmytoises to Steelshell for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10762','9883'); -- Changed Warden Beetles to Chamber Beetle for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10763','9899'); -- Changed Kabooms to Hellmine for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10764','9885'); -- Changed Fortalice Bats to Citadel Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10765','9832'); -- Changed King Crawlers to Soldier Crawler for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10766','9833'); -- Changed Vespos to Soul Stinger for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10767','9820'); -- Changed Dancing Jewels to Hornfly for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10768','9816'); -- Changed Olid Funguars to Exoray for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10769','9801'); -- Changed Accursed Soldiers to Wendigo for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10770','9803'); -- Changed Acursed Sorcerers to Wight for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10771','9778'); -- Changed Madfly to Gallinipper for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10772','9765'); -- Changed Rockmills to Amphisbaena for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10773','5890'); -- Changed Goblin Brigands to Goblin Mugger for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10774','9559'); -- Changed Goblin Headsmen to Goblin Butcher for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10775','5889'); -- Changed Goblin Healers to Goblin Leecher for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10776','9573'); -- Changed Witchetty Grubs to Stone Eater for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10777','9574'); -- Changed Couloir Leech to Thread Leech for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10778','9577'); -- Changed Prim Pikas to Wadi Hare for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10779','6701'); -- Changed Natty Gibbons to Young Opo-opo for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10780','5803'); -- Changed Trimmers to Snipper for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10781','9571'); -- Changed Fume Lizards to Steam Lizzard for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10782','5888'); -- Changed Goblin Conjurers to Goblin Gambler for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10783','9565'); -- Changed Goblin Bladesmiths to Goblin Tinkerer for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10784','9558'); -- Changed Goblin Bushwhackers to Goblin Ambusher for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10785','8991'); -- Changed Lacerators to Clipper for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10786','9014'); -- Changed Spool Leeches to Thread Leech for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10787','8744'); -- Changed Hovering Oculus to Bat Eye for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10788','8765'); -- Changed Bilesucker to Stirge for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10789','8751'); -- Changed Goblin Hoodoo to Goblin Gambler for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10790','9628'); -- Changed Goblin Artificer to Goblin Smithy for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10791','9622'); -- Changed Goblin Tanner to Goblin Furrier for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10792','9626'); -- Changed Goblin Chaser to Goblin Pathfinder for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'8757','9629'); -- Changed Goblin's Bats to Goblin's Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10793','8456'); -- Changed Ossuary Worm to Cave Worm for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10794','8460'); -- Changed Ogre Bat to Dire Bat for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10795','190'); -- Changed Barrow Scorpion to Cutlass Scorpion for 75 content
update `mob_groups` set `minLevel` = '64' where `groupid` = 10796; -- Changed Bonnet Beetle min level to mimic Armet Beetle for 75 content
update `mob_groups` set `maxLevel` = '66' where `groupid` = 10796; -- Changed Bonnet Beetle min level to mimic Armet Beetle for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10797','9584'); -- Changed Troika Bats to Battue Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10798','16'); -- Changed Deathwatch Beetles to Beady Beetle for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10799','5693'); -- Changed Goblin Fleshers to Goblin Butcher for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10800','5702'); -- Changed Goblin Metallurgist to Goblin Tinkerer for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10801','9596'); -- Changed Goblin Lurchers to Goblin Mugger for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10802','9607'); -- Changed Skinnymalinks to WendigoWAR for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10803','9607'); -- Changed Skinnymajinx to WendigoBLM for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10804','9583'); -- Changed Covin Bats to Battle Bat for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10805','5691'); -- Changed Goblin Trailblazers to Goblin Ambusher for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10806','9708'); -- Changed Fetor Bats to Stink Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10807','9660'); -- Changed Fuligo to Black Slime for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10808','9663'); -- Changed Thorn Bats to Combat for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10809','9652'); -- Changed Buds Bunnies to Vorpal Bunny for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10810','6465'); -- Changed Bilis Leech to Poison Leech for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10811','9646'); -- Changed Swagger Spruces to Stalking Sapling for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10812','9631'); -- Changed Targe Beetles to Goliath Beetle for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10813','9759'); -- Changed Helbound Warriors to Tomb Warrior for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10814','9758'); -- Changed Hellbound Warlocks to Tomb Mage for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10815','9760'); -- Changed Nekros Hounds to Tomb Wolf for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10816','8842'); -- Changed Blackwater Pugils to Stygian Pugil for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10817','8840'); -- Changed Plunderer Crabs to Scavenger Crab for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10818','8830'); -- Changed Deviling Bats to Impish Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10819','8826'); -- Changed Sodden Bones to Fleshcraver for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10820','8835'); -- Changed Drowned Bones to Mindcraver for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10821','8841'); -- Changed Starborers to Starmite for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10822','8458'); -- Changed Rapier Scorions to Cutlass Scorpion for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10823','2213'); -- Changed Poroggo Excavators to Poroggo for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10824','4912'); -- Changed Flume Toads to Toad for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10825','9870'); -- Changed Bleeder Leeches to Poison Leech for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10826','9872'); -- Changed Chaser Bats to Seeker Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10827','9841'); -- Changed Warren Bats to Ancient Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10828','9845'); -- Changed Crypterpillars to Caterchipillar for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10829','8164'); -- Changed Visclaws to Steelshells for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10830','9267'); -- Changed Aura Sculpture to Aura Statue for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10831','9897'); -- Changed Donjon Bats to Funnel Bats for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10832','131'); -- Changed Babaulas to Doom Soldier for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10833','130'); -- Changed Boribabas to Doom Mage for 75 content
update `mob_groups` set `minLevel` = '73' where `groupid` = 10835; -- Changed Baelfyr min level for 75 content
update `mob_groups` set `maxLevel` = '74' where `groupid` = 10835; -- Changed Baelfyr min level for 75 content
update `mob_groups` set `minLevel` = '73' where `groupid` = 10836; -- Changed Byrgen min level for 75 content
update `mob_groups` set `maxLevel` = '74' where `groupid` = 10836; -- Changed Byrgen min level for 75 content
update `mob_groups` set `minLevel` = '73' where `groupid` = 10837; -- Changed Gefyrst min level for 75 content
update `mob_groups` set `maxLevel` = '74' where `groupid` = 10837; -- Changed Gefyrst min level for 75 content
update `mob_groups` set `minLevel` = '73' where `groupid` = 10838; -- Changed Ungeweder min level for 75 content
update `mob_groups` set `maxLevel` = '74' where `groupid` = 10838; -- Changed Ungeweder min level for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10839','120'); -- Changed Bight Rarab to Tropical Rarab for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10840','77'); -- Changed Camelopard to Catoblepas for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10841','116'); -- Changed Hypnos Eft to Tartarus Eft for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10842','156'); -- Changed Scowlenkos to Smolenkos for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10848','118'); -- Changed Seaboard Vulture to Toucan for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10849','6772'); -- Changed Aqueduct Spider to Desert Spider for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10850','1728'); -- Changed Aqueduct Spider to Aht Urhgan Attercop for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10851','791'); -- Changed Blazedrake to Ignidrake for 75 content
update `mob_spawn_points` set `groupid` = replace(`groupid`,'10852','821'); -- Changed Darner to Hawker for 75 content



SELECT * FROM dspdb.mob_groups where minLevel > 75 ORDER BY minLevel;