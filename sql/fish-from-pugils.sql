USE dspdb;

-- black eel lv 47 for eel kabob, heavily nerfed selprice
-- black sole super huge for sushi, heavily nerfed selprice
-- crayfish, garbage fish, drops a lot
-- bastore sardine lv 11 great low lv, needed for fish mithk
-- moat carp lv 11 roast carp, carp sushi, sort of useful
-- cheval salmon, kinda useful lv 21
-- shining trout lv 37 for roast trout, not very useful
-- Pipira lv 27 not super impt
-- quus lv 19 REALLY not impt
-- gold lobster lv 46 super rare
-- bluetail lv 55 needed for fish mithk (nerfed sellpr)
-- bastore bream lv 86 used in some high up cooking, like bream sushi (nerfed sellpr)
-- giant catfish lv 31 pretty useless, only used for Steamed Catfish
-- red terrapin lv 53 just used for turtle soup
-- dark bass lv 33 low resale, just used for 1 PITA cooking recipe
-- emperor fish lv 91, used for an easy 82 ck recipe, SUPER RARE
-- zafmlug bass lv 40ish only used for one 97 ck recipe

-- ~~~blist~~~
-- bladefish
-- crescent fish
-- crystal bass
-- gold carp
-- monke-onke
-- ogre eel
-- tricolored carp


/*
INSERT INTO mob_droplist VALUES (2849,0,0,1000,4472,500); -- crayfish, shining trout and cheval salmon to ERONF
INSERT INTO mob_droplist VALUES (2849,0,0,1000,4379,250);
INSERT INTO mob_droplist VALUES (2849,0,0,1000,4354,250);
*/

/*
INSERT INTO mob_droplist VALUES (1140,0,0,1000,4472,600); -- crayfish, shining trout and cheval salmon to GHELSBA
INSERT INTO mob_droplist VALUES (1140,0,0,1000,4379,280);
INSERT INTO mob_droplist VALUES (1140,0,0,1000,4379,120);
INSERT INTO mob_droplist VALUES (1140,0,0,1000,4354,280);
INSERT INTO mob_droplist VALUES (1140,0,0,1000,4354,120);
*/

/*
INSERT INTO mob_droplist VALUES (2857,0,0,1000,4360,300);
INSERT INTO mob_droplist VALUES (2857,0,0,1000,4399,100);
INSERT INTO mob_droplist VALUES (2857,0,0,1000,4472,100);
INSERT INTO mob_droplist VALUES (2857,0,0,1000,4383,20);
INSERT INTO mob_droplist VALUES (2857,0,0,1000,4401,50);
INSERT INTO mob_droplist VALUES (2857,0,0,1000,4464,150);
INSERT INTO mob_droplist VALUES (2857,0,0,1000,4514,150);

-- E SARUTA (above):

-- bastore sardine 30%
-- bluetail 10%
-- crayfish 10%
-- gold lobster 2%
-- moat carp 50%
-- pipira 15%
-- quus 15%

-- 4360
-- 4399
-- 4472
-- 4383
-- 4401
-- 4464
-- 4514

*/

-- now putting comments above ~~~~~~~~~~~~~~~~~

/*
Giddeus, Giant Pugil

-- crayfish 80%
-- crayfish 70%
-- giant catfish 35%
-- red terrapin 15%

4472
4469
4402

*/
/*
INSERT INTO mob_droplist VALUES (1133,0,0,1000,4472,800);
INSERT INTO mob_droplist VALUES (1133,0,0,1000,4472,700);
INSERT INTO mob_droplist VALUES (1133,0,0,1000,4469,350);
INSERT INTO mob_droplist VALUES (1133,0,0,1000,4402,150);
*/

/*
CL, three pugil types for each landing, spinous nerfed

-- dark bass 25%
-- giant catfish 20%
-- muddy siredon 15%
-- phanauet newt 75%
-- shining trout 40%

4428
4469
5126
5125
4354
*/
/*
INSERT INTO mob_droplist VALUES (2019,0,0,1000,4428,250);
INSERT INTO mob_droplist VALUES (2019,0,0,1000,4469,200);
INSERT INTO mob_droplist VALUES (2019,0,0,1000,5126,150);
INSERT INTO mob_droplist VALUES (2019,0,0,1000,5125,750);
INSERT INTO mob_droplist VALUES (2019,0,0,1000,4354,400);
INSERT INTO mob_droplist VALUES (1035,0,0,1000,4428,250);
INSERT INTO mob_droplist VALUES (1035,0,0,1000,4469,200);
INSERT INTO mob_droplist VALUES (1035,0,0,1000,5126,150);
INSERT INTO mob_droplist VALUES (1035,0,0,1000,5125,750);
INSERT INTO mob_droplist VALUES (1035,0,0,1000,4354,400);
INSERT INTO mob_droplist VALUES (3308,0,0,1000,4428,200);
INSERT INTO mob_droplist VALUES (3308,0,0,1000,4469,110);
INSERT INTO mob_droplist VALUES (3308,0,0,1000,5126,150);
INSERT INTO mob_droplist VALUES (3308,0,0,1000,5125,750);
INSERT INTO mob_droplist VALUES (3308,0,0,1000,4354,230);
*/

/*
Juger Forest, Land Pugil

-- cheval salmon 18%
-- crayfish 40%
-- dark bass 9%
-- emperor fish 7%
-- giant catfish 20%
-- moat carp 85%
-- red terrapin 11%

4379
4472
4428
4454
4469
4401
4402
*/
/*
INSERT INTO mob_droplist VALUES (2022,0,0,1000,4379,180);
INSERT INTO mob_droplist VALUES (2022,0,0,1000,4472,400);
INSERT INTO mob_droplist VALUES (2022,0,0,1000,4428,90);
INSERT INTO mob_droplist VALUES (2022,0,0,1000,4454,70);
INSERT INTO mob_droplist VALUES (2022,0,0,1000,4469,200);
INSERT INTO mob_droplist VALUES (2022,0,0,1000,4401,850);
INSERT INTO mob_droplist VALUES (2022,0,0,1000,4402,110);
*/

/*
Pashhow, Land Pugil

-- cheval salmon 18%
-- crayfish 40%
-- copper frog 55%
-- copper frog 35%
-- emperor fish 7%
-- giant catfish 20%
-- moat carp 85%
-- red terrapin 11%

4379
4472
4515
4515
4454
4469
4401
4402
*/
/*
INSERT INTO mob_droplist VALUES (2024,0,0,1000,4379,180);
INSERT INTO mob_droplist VALUES (2024,0,0,1000,4472,400);
INSERT INTO mob_droplist VALUES (2024,0,0,1000,4515,550);
INSERT INTO mob_droplist VALUES (2024,0,0,1000,4515,350);
INSERT INTO mob_droplist VALUES (2024,0,0,1000,4454,70);
INSERT INTO mob_droplist VALUES (2024,0,0,1000,4469,200);
INSERT INTO mob_droplist VALUES (2024,0,0,1000,4401,850);
INSERT INTO mob_droplist VALUES (2024,0,0,1000,4402,110);
*/

/*
Bead, Land Pugil and Big Jaw (buffed)

-- cheval salmon 18%
-- crayfish 40%
-- copper frog 55%
-- emperor fish 7%
-- giant catfish 20%
-- moat carp 85%
-- red terrapin 11%

4379
4472
4515
4454
4469
4401
4402
*/
/*
INSERT INTO mob_droplist VALUES (2027,0,0,1000,4379,180);
INSERT INTO mob_droplist VALUES (2027,0,0,1000,4472,400);
INSERT INTO mob_droplist VALUES (2027,0,0,1000,4515,550);
INSERT INTO mob_droplist VALUES (2027,0,0,1000,4454,70);
INSERT INTO mob_droplist VALUES (2027,0,0,1000,4469,200);
INSERT INTO mob_droplist VALUES (2027,0,0,1000,4401,850);
INSERT INTO mob_droplist VALUES (2027,0,0,1000,4402,110);

INSERT INTO mob_droplist VALUES (4452,0,0,1000,4379,350);
INSERT INTO mob_droplist VALUES (4452,0,0,1000,4472,200);
INSERT INTO mob_droplist VALUES (4452,0,0,1000,4515,850);
INSERT INTO mob_droplist VALUES (4452,0,0,1000,4454,120);
INSERT INTO mob_droplist VALUES (4452,0,0,1000,4469,450);
INSERT INTO mob_droplist VALUES (4452,0,0,1000,4401,890);
INSERT INTO mob_droplist VALUES (4452,0,0,1000,4402,260);
*/

/*
Davoi, Land Pugil

-- cheval salmon 12%
-- dark bass 15%
-- emperor fish 2%
-- giant catfish 30%
-- moat carp 80%
-- moat carp 60%
-- red terrapin 16%

4379
4428
4454
4469
4401
4401
4402
*/
/*
INSERT INTO mob_droplist VALUES (650,0,0,1000,4379,120);
INSERT INTO mob_droplist VALUES (650,0,0,1000,4428,150);
INSERT INTO mob_droplist VALUES (650,0,0,1000,4454,20);
INSERT INTO mob_droplist VALUES (650,0,0,1000,4469,300);
INSERT INTO mob_droplist VALUES (650,0,0,1000,4401,800);
INSERT INTO mob_droplist VALUES (650,0,0,1000,4401,600);
INSERT INTO mob_droplist VALUES (650,0,0,1000,4402,160);
*/

/*
D00nz, Beach Pugil

-- cobalt jellyfish 85%
-- cobalt jellyfish 45%
-- bastore sardine 70%
-- quus 30%
-- zafmlug bass 15%

4360
4360
4443
4514
4385
*/
/*
INSERT INTO mob_droplist VALUES (267,0,0,1000,4360,850);
INSERT INTO mob_droplist VALUES (267,0,0,1000,4360,450);
INSERT INTO mob_droplist VALUES (267,0,0,1000,4443,700);
INSERT INTO mob_droplist VALUES (267,0,0,1000,4514,300);
INSERT INTO mob_droplist VALUES (267,0,0,1000,4385,150);
*/

/*
Buburimu, Shoal Pugil

-- bluetail 32%
-- yellow globe 80%
-- yellow globe 30%

4399
4403

*/
/*
INSERT INTO mob_droplist VALUES (3188,0,0,1000,4399,320);
INSERT INTO mob_droplist VALUES (3188,0,0,1000,4403,800);
INSERT INTO mob_droplist VALUES (3188,0,0,1000,4403,300);
*/

/*
Bibiki Bay, Jagil

-- Bastore Sardine 60%
-- Cobalt Jellyfish 40%
-- Cone Calamary 88.5%
-- Cone Calamary 66%
-- Cone Calamary 12%
-- Cone Calamary 6%
-- Zafmlug Bass 30%

4360
4443
5128
5128
5128
4385
*/
/*
INSERT INTO mob_droplist VALUES (4497,0,0,1000,4360,600);
INSERT INTO mob_droplist VALUES (4497,0,0,1000,4443,400);
INSERT INTO mob_droplist VALUES (4497,0,0,1000,5128,885);
INSERT INTO mob_droplist VALUES (4497,0,0,1000,5128,660);
INSERT INTO mob_droplist VALUES (4497,0,0,1000,5128,120);
INSERT INTO mob_droplist VALUES (4497,0,0,1000,5128,60);
INSERT INTO mob_droplist VALUES (4497,0,0,1000,4385,300);
*/

/*
Qufim Island, Greater Pugil

-- black sole 35%
-- black sole 10%
-- bluetail 23%
-- cobalt jellyfish 33%
-- Cone Calamary 8%
-- Cone Calamary 3%
-- gigant squid 4%
-- nosteau herring 60%
-- three-eyed fish 2.5%
-- tiger cod 9%
-- yellow globe 19%

4384
4384
4399
4443
5128
5128
4474
4482
4478
4483
4403
*/
/*
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4384,350);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4384,100);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4399,230);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4443,330);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,5128,80);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,5128,30);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4474,40);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4482,600);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4478,25);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4483,90);
INSERT INTO mob_droplist VALUES (1572,0,0,1000,4403,190);
*/

/*
N Gustaberg, Sand Pugil

-- black eel 80%
-- black eel 45%
-- copper frog 70%
-- copper frog 20%

4429
4429
4515
4515
*/
/*
INSERT INTO mob_droplist VALUES (3049,0,0,1000,4429,800);
INSERT INTO mob_droplist VALUES (3049,0,0,1000,4429,450);
INSERT INTO mob_droplist VALUES (3049,0,0,1000,4515,700);
INSERT INTO mob_droplist VALUES (3049,0,0,1000,4515,200);
*/

/*
Yuh Jung, Makara

-- forest carp 90%
-- forest carp 60%
-- pipira 90%
-- pipira 45%
-- elshimo frog 20%
-- elshimo newt 30%

4289
4289
4464
4464
4290
4579

*/
/*
INSERT INTO mob_droplist VALUES (2139,0,0,1000,4289,900);
INSERT INTO mob_droplist VALUES (2139,0,0,1000,4289,600);
INSERT INTO mob_droplist VALUES (2139,0,0,1000,4464,900);
INSERT INTO mob_droplist VALUES (2139,0,0,1000,4464,450);
INSERT INTO mob_droplist VALUES (2139,0,0,1000,4290,200);
INSERT INTO mob_droplist VALUES (2139,0,0,1000,4579,300);
*/

/*
Yhoat Jung, Big Jaw

very similar to above, just buffed

*/
/*
INSERT INTO mob_droplist VALUES (301,0,0,1000,4289,880);
INSERT INTO mob_droplist VALUES (301,0,0,1000,4289,700);
INSERT INTO mob_droplist VALUES (301,0,0,1000,4464,870);
INSERT INTO mob_droplist VALUES (301,0,0,1000,4464,620);
INSERT INTO mob_droplist VALUES (301,0,0,1000,4290,400);
INSERT INTO mob_droplist VALUES (301,0,0,1000,4579,550);
*/

/*
Gusgen Mines, Greater Pugil

-- black eel 90%
-- black eel 60%
-- black eel 25%
-- black eel 10%
-- copper frog 30%
-- copper frog 8%
-- crayfish 40%

4429
4429
4429
4429
4515
4515
4472

*/
/*
INSERT INTO mob_droplist VALUES (1575,0,0,1000,4429,900);
INSERT INTO mob_droplist VALUES (1575,0,0,1000,4429,600);
INSERT INTO mob_droplist VALUES (1575,0,0,1000,4429,250);
INSERT INTO mob_droplist VALUES (1575,0,0,1000,4429,100);
INSERT INTO mob_droplist VALUES (1575,0,0,1000,4515,300);
INSERT INTO mob_droplist VALUES (1575,0,0,1000,4515,80);
INSERT INTO mob_droplist VALUES (1575,0,0,1000,4472,400);
*/

/*
Gustav Tunnel, Makara and Demonic Pugil (buffed)

-- black eel 25%
-- copper frog 10%
-- moat carp 15%
-- quus 5%
-- sandfish 90%
-- sandfish 80%
-- sandfish 60%
-- sandfish 60%
-- sandfish 25%
-- sandfish 8%

4429
4515
4401
4514
4291
4291
4291
4291
4291
4291

*/
/*
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4429,250);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4515,100);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4401,150);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4514,50);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4291,900);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4291,800);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4291,600);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4291,600);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4291,250);
INSERT INTO mob_droplist VALUES (2142,0,0,1000,4291,80);

INSERT INTO mob_droplist VALUES (684,0,0,1000,4429,320);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4515,170);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4401,250);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4514,150);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4291,900);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4291,800);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4291,700);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4291,660);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4291,450);
INSERT INTO mob_droplist VALUES (684,0,0,1000,4291,180);
*/

/*
Den of Rancor, Demonic Pugil

-- bluetail 80%
-- bluetail 30%
-- cobalt jellyfish 20%
-- coral fragment 20%
-- coral fragment 8%
-- coral fragment 8%
-- coral fragment 8%
-- coral fragment 8%
-- coral fragment 8%
-- gold lobster 45%
-- quus 10%

4399
4399
4443
887
887
4383
4514

*/
/*
INSERT INTO mob_droplist VALUES (4461,0,0,1000,4399,800);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,4399,300);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,4443,200);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,887,200);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,887,80);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,887,80);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,887,80);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,887,80);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,887,80);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,4383,450);
INSERT INTO mob_droplist VALUES (4461,0,0,1000,4514,100);
*/

/*
SSG, Grotto Pugil and Razorjaw Pugil (buffed)

-- bastore bream 40%
-- bastore sardine 40%
-- bluetail 20%
-- cobalt jellyfish 50%
-- nebimonite 25%
-- silver shark 5%

4461
4360
4399
4443
4361
4451

*/
/*
INSERT INTO mob_droplist VALUES (1587,0,0,1000,4514,400);
INSERT INTO mob_droplist VALUES (1587,0,0,1000,4360,400);
INSERT INTO mob_droplist VALUES (1587,0,0,1000,4399,200);
INSERT INTO mob_droplist VALUES (1587,0,0,1000,4443,500);
INSERT INTO mob_droplist VALUES (1587,0,0,1000,4361,250);
INSERT INTO mob_droplist VALUES (1587,0,0,1000,4451,50);

INSERT INTO mob_droplist VALUES (2934,0,0,1000,4514,800);
INSERT INTO mob_droplist VALUES (2934,0,0,1000,4514,350);
INSERT INTO mob_droplist VALUES (2934,0,0,1000,4360,450);
INSERT INTO mob_droplist VALUES (2934,0,0,1000,4399,520);
INSERT INTO mob_droplist VALUES (2934,0,0,1000,4443,700);
INSERT INTO mob_droplist VALUES (2934,0,0,1000,4361,400);
INSERT INTO mob_droplist VALUES (2934,0,0,1000,4451,90);
*/

/*
Misareaux Coast, Makara

-- dark bass 25%
-- moat carp 90%
-- moat carp 60%
-- moat carp 60%
-- moat carp 60%
-- quus 25%

4428
4401
4401
4401
4401
4514

*/
/*
INSERT INTO mob_droplist VALUES (2136,0,0,1000,4428,250);
INSERT INTO mob_droplist VALUES (2136,0,0,1000,4401,900);
INSERT INTO mob_droplist VALUES (2136,0,0,1000,4401,600);
INSERT INTO mob_droplist VALUES (2136,0,0,1000,4401,600);
INSERT INTO mob_droplist VALUES (2136,0,0,1000,4401,600);
INSERT INTO mob_droplist VALUES (2136,0,0,1000,4514,250);
*/

/*
Cape Terrigan, Terror Pugil

-- bastore sardine 75%
-- bastore sardine 55%
-- bastore sardine 55%
-- bastore sardine 55%
-- shall shell 90%
-- shall shell 45%
-- quus 25%
-- zafmlug bass 20%

4360
4360
4360
4360
4484
4484
4514
4385
*/
/*
INSERT INTO mob_droplist VALUES (3453,0,0,1000,4360,750);
INSERT INTO mob_droplist VALUES (3453,0,0,1000,4360,550);
INSERT INTO mob_droplist VALUES (3453,0,0,1000,4360,550);
INSERT INTO mob_droplist VALUES (3453,0,0,1000,4360,550);
INSERT INTO mob_droplist VALUES (3453,0,0,1000,4484,900);
INSERT INTO mob_droplist VALUES (3453,0,0,1000,4484,450);
INSERT INTO mob_droplist VALUES (3453,0,0,1000,4514,250);
INSERT INTO mob_droplist VALUES (3453,0,0,1000,4385,200);
*/

/*
Toraimorai, Makara

-- coral fragment 7%
-- crayfish 60%
-- moat carp 55%
-- moat carp 55%
-- moat carp 55%
-- moat carp 55%
-- moat carp 55%
-- moat carp 55%
-- moat carp 55%

887
4472
4401
4401
4401
4401
4401
4401
4401

*/
/*
INSERT INTO mob_droplist VALUES (2141,0,0,1000,887,70);
INSERT INTO mob_droplist VALUES (2141,0,0,1000,4472,600);
INSERT INTO mob_droplist VALUES (2141,0,0,1000,4401,550);
INSERT INTO mob_droplist VALUES (2141,0,0,1000,4401,550);
INSERT INTO mob_droplist VALUES (2141,0,0,1000,4401,550);
INSERT INTO mob_droplist VALUES (2141,0,0,1000,4401,550);
INSERT INTO mob_droplist VALUES (2141,0,0,1000,4401,550);
INSERT INTO mob_droplist VALUES (2141,0,0,1000,4401,550);
INSERT INTO mob_droplist VALUES (2141,0,0,1000,4401,550);
*/

/*
Phomiuna Aqueducts, Big Jaw and Makara (buffed)

-- coral fragment 7%
-- crayfish 60%
-- crayfish 60%
-- crayfish 60%
-- crayfish 60%
-- crayfish 60%
-- crayfish 60%
-- crayfish 60%
-- lik 3%

887
4472
5129

*/
/*
INSERT INTO mob_droplist VALUES (300,0,0,1000,887,70);
INSERT INTO mob_droplist VALUES (300,0,0,1000,4472,600);
INSERT INTO mob_droplist VALUES (300,0,0,1000,4472,600);
INSERT INTO mob_droplist VALUES (300,0,0,1000,4472,600);
INSERT INTO mob_droplist VALUES (300,0,0,1000,4472,600);
INSERT INTO mob_droplist VALUES (300,0,0,1000,4472,600);
INSERT INTO mob_droplist VALUES (300,0,0,1000,4472,600);
INSERT INTO mob_droplist VALUES (300,0,0,1000,4472,600);
INSERT INTO mob_droplist VALUES (300,0,0,1000,5129,30);

INSERT INTO mob_droplist VALUES (2137,0,0,1000,887,230);
INSERT INTO mob_droplist VALUES (2137,0,0,1000,4472,900);
INSERT INTO mob_droplist VALUES (2137,0,0,1000,4472,700);
INSERT INTO mob_droplist VALUES (2137,0,0,1000,4472,700);
INSERT INTO mob_droplist VALUES (2137,0,0,1000,4472,700);
INSERT INTO mob_droplist VALUES (2137,0,0,1000,4472,700);
INSERT INTO mob_droplist VALUES (2137,0,0,1000,4472,700);
INSERT INTO mob_droplist VALUES (2137,0,0,1000,4472,700);
INSERT INTO mob_droplist VALUES (2137,0,0,1000,5129,35);
*/

/*
mamook, suhur mas

-- alabaligi 90%
-- alabaligi 90%
-- alabaligi 70%
-- alabaligi 70%
-- alabaligi 60%

5461
*/
/*
INSERT INTO mob_droplist VALUES (3404,0,0,1000,5461,900);
INSERT INTO mob_droplist VALUES (3404,0,0,1000,5461,900);
INSERT INTO mob_droplist VALUES (3404,0,0,1000,5461,700);
INSERT INTO mob_droplist VALUES (3404,0,0,1000,5461,700);
INSERT INTO mob_droplist VALUES (3404,0,0,1000,5461,600);
*/

/*
mount zhayolm, sicklemoon jagil

-- deniz 2%
-- kalamar 90%
-- kalamar 80%
-- kalamar 70%
-- kalamar 60%
-- kalamar 60%
-- kalamar 20%
-- shall shell 65%

5447
5448
4484

*/
/*
INSERT INTO mob_droplist VALUES (4465,0,0,1000,5447,25);
INSERT INTO mob_droplist VALUES (4465,0,0,1000,5448,900);
INSERT INTO mob_droplist VALUES (4465,0,0,1000,5448,800);
INSERT INTO mob_droplist VALUES (4465,0,0,1000,5448,700);
INSERT INTO mob_droplist VALUES (4465,0,0,1000,5448,600);
INSERT INTO mob_droplist VALUES (4465,0,0,1000,5448,600);
INSERT INTO mob_droplist VALUES (4465,0,0,1000,5448,200);
INSERT INTO mob_droplist VALUES (4465,0,0,1000,4484,650);
*/


SELECT mob_droplist.dropId,mob_droplist.itemId,item_basic.name,mob_droplist.itemRate
FROM mob_droplist INNER JOIN item_basic ON mob_droplist.itemId = item_basic.itemid
WHERE itemRate > 0 AND mob_droplist.dropId IN (4465);