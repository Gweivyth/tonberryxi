-- replace Pugil with your mob's name
-- replace the zoneip with your map server's IP address

USE dspdb;

SELECT mob_droplist.dropId,mob_droplist.dropType,mob_droplist.itemId,item_basic.name,mob_droplist.itemRate,
mob_groups.zoneid,zone_settings.name,mob_groups.minLevel,mob_groups.maxLevel
FROM mob_droplist
INNER JOIN item_basic ON mob_droplist.itemId = item_basic.itemid
INNER JOIN mob_groups ON mob_droplist.dropId = mob_groups.dropid
INNER JOIN zone_settings ON zone_settings.zoneid = mob_groups.zoneid
WHERE mob_groups.dropId IN
(
SELECT mob_groups.dropid FROM mob_groups
INNER JOIN zone_settings ON mob_groups.zoneid = zone_settings.zoneid
WHERE mob_groups.groupid IN
(SELECT mob_spawn_points.groupid FROM mob_spawn_points WHERE mob_spawn_points.mobname LIKE 'Pugil' AND (pos_x NOT IN (0.000) AND pos_y NOT IN (0.000) AND pos_z NOT IN (0.000)))
AND mob_groups.zoneid IN (SELECT zone_settings.zoneid FROM zone_settings WHERE zoneip IN ('127.0.0.1'))
)
;