-----------------------------------
-- Area: Bastok Mines
--  NPC: Explorer Moogle
-----------------------------------
require("scripts/globals/teleports")
-----------------------------------

local eventId = 585

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
	if player:isWeekendEvent() then
		dsp.teleport.explorerMoogleOnTrigger(player, eventId)
		--player:startEvent(10114);
	end
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
    dsp.teleport.explorerMoogleOnEventFinish(player, csid, option, eventId)
end