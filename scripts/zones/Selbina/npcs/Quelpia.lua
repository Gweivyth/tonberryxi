-----------------------------------
-- Area: Selbina
--  NPC: Quelpia
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Selbina/IDs")
require("scripts/globals/shop")
-----------------------------------

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        4610,   585,    -- Scroll of Cure II
        4611,  2861,    -- Scroll of Cure III
        4616,  8180,    -- Scroll of Curaga II
        4620,  4578,    -- Scroll of Raise
        4629, 27500,    -- Scroll of Holy
        4632,  8080,    -- Scroll of Dia II
        4637,  7600,    -- Scroll of Banish II
        4652,  6066,    -- Scroll of Protect II
        4657, 13840,    -- Scroll of Shell II
        4665, 15200,    -- Scroll of Haste
        4708,  4444,    -- Scroll of Enfire
        4709,  3288,    -- Scroll of Enblizzard
        4710,  2050,    -- Scroll of Enaero
        4711,  1627,    -- Scroll of Enstone
        4712,  1263,    -- Scroll of Enthunder
        4713,  5786,    -- Scroll of Enwater
    }

    player:showText(npc, ID.text.QUELPIA_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
