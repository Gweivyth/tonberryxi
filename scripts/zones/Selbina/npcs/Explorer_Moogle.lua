-----------------------------------
-- Area: Selbina
--  NPC: Explorer Moogle
-----------------------------------
require("scripts/globals/teleports")
-----------------------------------

local eventId = 1135

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
	if player:isWeekendEvent() then
		dsp.teleport.explorerMoogleOnTrigger(player, eventId)
	end
	--local bool = player:isWeekendEvent()
	--player:PrintToPlayer(string.format("%i",bool))
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
    dsp.teleport.explorerMoogleOnEventFinish(player, csid, option, eventId)
end