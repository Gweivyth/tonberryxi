-----------------------------------
-- Area: Selbina
--  NPC: Torapiont
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Selbina/IDs")
require("scripts/globals/shop")
-----------------------------------

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        16411,  3447,    -- Claws 30
        16451,  2604,    -- Mythril Dagger 23
        16513,  4055,    -- Tuck 23
        16584, 10584,    -- Mythril Claymore 36
        16643,  4671,    -- Battleaxe 20
        16705,  2219,    -- Greataxe 12
        17050,   333,    -- Willow Wand
        17051,   605,    -- Yew Wand
        17089,   484,    -- Holly Staff
        17307,     9,    -- Dart
        17336,     5,    -- Crossbow Bolt
        17318,     3,    -- Wooden Arrow
        17320,     7,    -- Iron Arrow
    }

    player:showText(npc, ID.text.TORAPIONT_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
