-----------------------------------
-- Area: Lower Jeuno
--  NPC: Chetak
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Lower_Jeuno/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12466,  4895,    -- Red Cap 36
        12467,  9152,    -- Wool Cap 48
        12474,  3750,    -- Wool Hat 28
        12594,  7850,    -- Gambison 36
        12610, 8303,    -- Cloak 34
        12595, 13728,    -- Wool Gambison 48
        12602,  6175,    -- Wool Robe 28
        12609,  3769,    -- Black Tunic 20
        12722,  4280,    -- Bracers 36
        12738,  3933,    -- Linen Mitts 34
        12730,  3389,    -- Wool Cuffs 28
        12737,  1712,    -- White Mitts 20
    }

    player:showText(npc, ID.text.CHETAK_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
