-----------------------------------
-- Area: Lower Jeuno
--  NPC: Chenokih
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Lower_Jeuno/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12850,  6120,    -- Hose 36
        12866,  5658,    -- Linen Slacks 34
        12851, 11520,    -- Wool Hose 48
        12858,  4783,    -- Wool Slops 28
        12865,  2411,    -- Black Slacks 20
        12978,  3275,    -- Socks 36
        12994,  3588,    -- Shoes 34
        12979,  6540,    -- Wool Socks 48
        12986,  2894,    -- Chestnut Sabots 28
        12993,  1233,    -- Sandals 20
        13577,  8820,    -- Black Cape
        13568,   750,    -- Scarlet Ribbon
    }

    player:showText(npc, ID.text.CHENOKIH_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
