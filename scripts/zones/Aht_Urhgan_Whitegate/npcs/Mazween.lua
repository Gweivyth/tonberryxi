-----------------------------------
-- Area: Aht Urhgan Whitegate
--  NPC: Mazween
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Aht_Urhgan_Whitegate/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        --[[
		4881, 11200,    -- Scroll of Sleepga
        4867, 18720,    -- Scroll of Sleep II
        4829, 25200,    -- Poison II
        4839, 14000,    -- Bio II
        4833,  5160,    -- Poisonga
        4769, 19932,    -- Stone III
        4779, 22682,    -- Water III
        4764, 27744,    -- Aero III
        4754, 33306,    -- Fire III
        4759, 39368,    -- Blizzard III
        4774, 45930,    -- Thunder III
        --4883, 27000,    -- Absorb-TP
        --4854, 30780,    -- Drain II
        --4885, 70560,    -- Dread Spikes
        --4886, 44000,    -- Absorb-ACC
        --4856, 79800     -- Aspir II
		]]--
		4883, 21200,    -- Absorb-TP
		4854, 25690,    -- Drain II
		4885, 52915,    -- Dread Spikes
		
		4867,   8120,    -- Scroll of Sleep II
        4881,  10150,    -- Scroll of Sleepga
        4769,  17932,    -- Scroll of Stone III
        4770,  78000,    -- Scroll of Stone IV
        4798,  11000,    -- Scroll of Stonega II
        4799,  59875,    -- Scroll of Stonega III
        4779,  19682,    -- Scroll of Water III
        4780,  85000,    -- Scroll of Water IV
        4808,  12050,    -- Scroll of Waterga II
        4809,  72250,    -- Scroll of Waterga III
        4764,  24344,    -- Scroll of Aero III
        4765,  97750,    -- Scroll of Aero IV
        4793,  17800,    -- Scroll of Aeroga II
        4794,  85625,    -- Scroll of Aeroga III
        4754,  28506,    -- Scroll of Fire III
        4755, 109500,    -- Scroll of Fire IV
        4783,  19307,    -- Scroll of Firaga II
        4784, 101000,    -- Scroll of Firaga III
        4759,  34368,    -- Scroll of Blizzard III
        4760, 120250,    -- Scroll of Blizzard IV
        4788,  23244,    -- Scroll of Blizzaga II
        4789, 109800,    -- Scroll of Blizzaga III
        4774,  39430,    -- Scroll of Thunder III
        4775, 142000,    -- Scroll of Thunder IV
        4803,  27681,    -- Scroll of Thundaga II
        4804, 129000,    -- Scroll of Thundaga III
		
    }

    player:showText(npc, ID.text.MAZWEEN_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
