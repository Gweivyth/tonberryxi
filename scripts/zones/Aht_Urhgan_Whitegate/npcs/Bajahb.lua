-----------------------------------
-- Area: Aht Urhgan Whitegate
--  NPC: Bajahb
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Aht_Urhgan_Whitegate/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12424, 6210,    -- Iron Mask
        12552, 8950,    -- Chainmail
        12680,  4925,    -- Chain Mittens
        12808, 7365,    -- Chain Hose
        12936,  4630,     -- Greaves
    }

    player:showText(npc, ID.text.BAJAHB_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
