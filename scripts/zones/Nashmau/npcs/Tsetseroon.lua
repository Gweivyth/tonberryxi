-----------------------------------
-- Area: Nashmau
--  NPC: Tsetseroon
-- Standard Info NPC
-----------------------------------

function onTrade(player,npc,trade)
end;

function onTrigger(player,npc)
    player:startEvent(4);
	
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
		--[[
		tension_spring_ii
		scanner
		loudspeaker_ii
		accelerator_ii
		armor_plate_ii
		stabilizer_ii
		mana_jammer_ii
		auto-repair_kit_ii
		mana_tank_ii
		attuner
		tactical_processor
		drum_magazine
		equalizer
		target_marker
		mana_channeler
		eraser
		smoke_screen
		reactive_shield
		tranquilizer
		turbo_charger
		schurzen
		dynamo
		condenser
		economizer
		hammermill
		coiler
		steam_jacket
		vivi-valve
		disruptor
		strobe_ii
		barrier_module
		]]
	local stock =
    {
		2241, 29640,--
		2244, 29640,--
		2245, 29640,--
		2249, 29640,--
		2253, 29640,--
		2257, 29640,--
		2261, 29640,--
		9068, 41496,--
		2265, 41496,--
		2269, 41496,--
		2412, 41496,--
		9032, 53352,--
		2322, 118560,--
		2323, 118560,--
		2324, 118560,--
		2325, 118560,--
		2326, 118560,--
		2327, 118560,--
		2328, 118560,--
		2329, 118560,--
		3313, 118560,--
		3314, 118560,--
		2413, 185250,--
		2414, 185250,--
		2347, 222300,--
		2348, 222300,--
		2349, 222300,--
		2350, 222300,--
		2351, 222300,--
		2352, 222300,--
		2354, 222300,--
		2353, 222300
    }

    dsp.shop.general(player, stock)
end;
