-----------------------------------
-- Area: Nashmau
--  NPC: Mamaroon
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Nashmau/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        4860,  37800,    -- Scroll of Stun
        4708,   2260,    -- Scroll of Enfire
        4709,   1998,    -- Scroll of Enblizzard
        4710,   1700,    -- Scroll of Enaero
        4711,   1630,    -- Scroll of Entone
        4712,   1515,    -- Scroll of Enthunder
        4713,   2474,    -- Scroll of Enwater
        4859,   8800,    -- Scroll of Shock Spikes
        2502,  29950,    -- White Puppet Turban
        2501,  29950,    -- Black Puppet Turban
        --4706, 100800,    -- Scroll of Enlight
        --4707, 100800,    -- Scroll of Endark
    }

    player:showText(npc, ID.text.MAMAROON_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
