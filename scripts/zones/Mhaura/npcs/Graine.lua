-----------------------------------
-- Area: Mhaura
--  NPC: Graine
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Mhaura/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12440,  202,    -- Leather Bandana
        12448,  121,    -- Bronze Cap
        12449,  653,    -- Brass Cap
        12568,  362,    -- Leather Vest
        12576,  366,    -- Bronze Harness
        12577, 1078,    -- Brass Harness
        12696,  194,    -- Leather Gloves
        12704,   78,    -- Bronze Mittens
        12705,  566,    -- Brass Mittens
        12824,  289,    -- Leather Trousesrs
        12832,  137,    -- Bronze Subligar
        12833,  820,    -- Brass Subligar
        12952,  185,    -- Leather Highboots
        12960,   72,    -- Bronze Leggings
        12961,  455,    -- Brass Leggings
    }

    player:showText(npc, ID.text.GRAINE_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
