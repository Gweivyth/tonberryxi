-----------------------------------
-- Area: Mhaura
--  NPC: Runito-Monito
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Mhaura/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        16405,  106,    -- Cat Bagnakhs
        16407,  912,    -- Brass Bagnakhs
        16449,  512,    -- Brass Dagger
        17059,   91,    -- Bronze Rod
        17081,  337,    -- Brass Rod
        16531, 1414,    -- Brass Xiphos
        16583, 1213,    -- Claymore
        16704,  525,    -- Butterfly Axe
        17307,    9,    -- Dart
        17318,    3,    -- Wooden Arrow
        17319,    4,    -- Bone Arrow
        17336,    5,    -- Crossbow Bolts
    }

    player:showText(npc, ID.text.RUNITOMONITO_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
