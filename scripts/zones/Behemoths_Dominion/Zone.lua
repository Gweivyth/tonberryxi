-----------------------------------
--
-- Zone: Behemoths_Dominion (127)
--
-----------------------------------
local ID = require("scripts/zones/Behemoths_Dominion/IDs");
require("scripts/globals/settings");
require("scripts/globals/zone");
-----------------------------------

function onInitialize(zone)
    if (LandKingSystem_NQ ~= 1) then
        UpdateNMSpawnPoint(ID.mob.ADAMANTOISE);
		UpdateNMSpawnPoint(ID.mob.ASPIDOCHELONE)
        --GetMobByID(ID.mob.FAFNIR):setRespawnTime(math.random(900, 10800));
		
		local re = GetServerVariable("BehemothRespawn")
		if os.time() < re then
			if GetServerVariable("BehemothSwitch") == 0 then
				GetMobByID(ID.mob.BEHEMOTH):setRespawnTime(re - os.time())
			else
				DisallowRespawn(ID.mob.KING_BEHEMOTH, false)
				DisallowRespawn(ID.mob.BEHEMOTH, true)
				GetMobByID(ID.mob.KING_BEHEMOTH):setRespawnTime(re - os.time())
			end
		else
			if GetServerVariable("BehemothSwitch") == 0 then
				SpawnMob(ID.mob.BEHEMOTH)
			else
				DisallowRespawn(ID.mob.KING_BEHEMOTH, false)
				DisallowRespawn(ID.mob.BEHEMOTH, true)
				SpawnMob(ID.mob.KING_BEHEMOTH)
			end
		end
    end
end;

function onConquestUpdate(zone, updatetype)
    dsp.conq.onConquestUpdate(zone, updatetype)
end;

function onZoneIn(player,prevZone)
    local cs = -1;
    if (player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0) then
        player:setPos(358.134,24.806,-60.001,123);
    end
    return cs;
end;

function onRegionEnter(player,region)
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;
