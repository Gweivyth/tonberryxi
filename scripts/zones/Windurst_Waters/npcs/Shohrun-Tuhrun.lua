-----------------------------------
-- Area: Windurst Waters
--  NPC: Shohrun-Tuhrun
-- Standard Merchant NPC
-- Confirmed shop stock, August 2013
-----------------------------------
require("scripts/globals/shop");
local ID = require("scripts/zones/Windurst_Waters/IDs");
-----------------------------------

function onTrade(player,npc,trade)
end;

function onTrigger(player,npc)
    player:showText(npc,ID.text.SHOHRUNTUHRUN_SHOP_DIALOG);

    stock = {
        4665, 15000,1,     --Haste

        4620,  4820,2,     --Scroll of Raise
        4632,  9500,2,     --Scroll of Dia II
        4637,  7320,2,     --Scroll of Banish II
        4652,  5715,2,     --Scroll of Protect II
        4657, 13910,2,     --Scroll of Shell II
        4708,  4356,2,     --Scroll of Enfire
        4709,  3512,2,     --Scroll of Enblizzard
        4710,  2101,2,     --Scroll of Enaero
        4711,  1735,2,     --Scroll of Enstone
        4712,  1212,2,     --Scroll of Enthunder
        4713,  5748,2,     --Scroll of Enwater

        4611,  2890,3,     --Scroll of Cure III
        4654, 66955,3,     --Scroll of Protect IV
        4736, 69800,3,     --Scroll of Protectra IV
        4868, 64400,3      --Scroll of Dispel
    }
    dsp.shop.nation(player, stock, dsp.nation.WINDURST);

end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;
