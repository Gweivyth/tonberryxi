-----------------------------------
-- Area: Windurst Waters
--  NPC: Orez-Ebrez
-- Standard Merchant NPC
-- Confirmed shop stock, August 2013
-----------------------------------
require("scripts/globals/shop");
local ID = require("scripts/zones/Windurst_Waters/IDs");
-----------------------------------

function onTrade(player,npc,trade)
end;

function onTrigger(player,npc)
    player:showText(npc,ID.text.OREZEBREZ_SHOP_DIALOG);

    stock = {
        12466,  4895,1,     --Red Cap 36
        12458,  2857,1,     --Soil Hachimaki 29
        12455,  3372,1,     --Beetle Mask 21

        12472,    91,2,     --Circlet 1
        12465,  3111,2,     --Cotton Headgear 23
        12440,   202,2,     --Leather Bandana 7
        12473,   879,2,     --Poet's Circlet 12
        12499,  5674,2,     --Flax Headband 36
        12457,  1318,2,     --Cotton Hachimaki 18
        12454,  1566,2,     --Bone Mask 16
        12474,  3258,2,     --Wool Hat 28

        12464,   842,3,     --Headgear 11
        12456,   317,3,     --Hachimaki 8
        12498,  1156,3,     --Cotton Headband 14
        12448,   121,3,     --Bronze Cap 1
        12449,   653,3      --Brass Cap 11
    }
    dsp.shop.nation(player, stock, dsp.nation.WINDURST);

end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;
