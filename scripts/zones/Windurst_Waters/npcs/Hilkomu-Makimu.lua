-----------------------------------
-- Area: Windurst Waters
--  NPC: Hilkomu-Makimu
-- Standard Merchant NPC
-- Confirmed shop stock, August 2013
-----------------------------------
require("scripts/globals/shop");
local ID = require("scripts/zones/Windurst_Waters/IDs");
-----------------------------------

function onTrade(player,npc,trade)
end;

function onTrigger(player,npc)
    player:showText(npc,ID.text.HIKOMUMAKIMU_SHOP_DIALOG);

    stock = {
        4829, 18200,1,     --Scroll of Poison II
        4839, 10951,1,     --Scroll of Bio II
        4833,  4190,1,     --Scroll of Poisonga

        4797,  1191,2,     --Scroll of Stonega
        4807,  2043,2,     --Scroll of Waterga
        4792,  4039,2,     --Scroll of Aeroga
        4782,  6781,2,     --Scroll of Firaga
        4787,  9548,2,     --Scroll of Blizzaga
        4802, 13456,2,     --Scroll of Thundaga
        4859,  7280,2,     --Scroll of Shock Spikes

        4768,  5314,3,     --Scroll of Stone II
        4778,  7400,3,     --Scroll of Water II
        4763, 11036,3,     --Scroll of Aero II
        4753, 14128,3,     --Scroll of Fire II
        4758, 19456,3,     --Scroll of Blizzard II
        4773, 23820,3      --Scroll of Thunder II
    }
    dsp.shop.nation(player, stock, dsp.nation.WINDURST);
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;
