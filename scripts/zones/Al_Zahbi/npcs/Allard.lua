-----------------------------------
-- Area: Al Zahbi
--  NPC: Allard
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Al_Zahbi/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12466, 4250,    --Red Cap
        12594, 7450,    --Gambison
        12722, 4025,    --Bracers
        12850, 5775,    --Hose
        12978, 2950    --Socks
    }

    player:showText(npc, ID.text.ALLARD_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
