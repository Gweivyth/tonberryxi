-----------------------------------
--
-- Zone: Dragons_Aery (154)
--
-----------------------------------
local ID = require("scripts/zones/Dragons_Aery/IDs");
require("scripts/globals/conquest");
require("scripts/globals/settings");
require("scripts/globals/zone");
-----------------------------------

function onInitialize(zone)
    if (LandKingSystem_NQ ~= 1) then
        UpdateNMSpawnPoint(ID.mob.FAFNIR);
		UpdateNMSpawnPoint(ID.mob.NIDHOGG)
        --GetMobByID(ID.mob.FAFNIR):setRespawnTime(math.random(900, 10800));
		
		local re = GetServerVariable("FafnirRespawn")
		if os.time() < re then
			if GetServerVariable("FafnirSwitch") == 0 then
				GetMobByID(ID.mob.FAFNIR):setRespawnTime(re - os.time())
			else
				DisallowRespawn(ID.mob.NIDHOGG, false)
				DisallowRespawn(ID.mob.FAFNIR, true)
				GetMobByID(ID.mob.NIDHOGG):setRespawnTime(re - os.time())
			end
		else
			if GetServerVariable("FafnirSwitch") == 0 then
				SpawnMob(ID.mob.FAFNIR)
			else
				DisallowRespawn(ID.mob.NIDHOGG, false)
				DisallowRespawn(ID.mob.FAFNIR, true)
				SpawnMob(ID.mob.NIDHOGG)
			end
		end
    end
end;

function onZoneIn(player,prevZone)
    local cs = -1;

    if (player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0) then
        player:setPos(-60.006,-2.915,-39.501,202);
    end

    return cs;
end;

function onConquestUpdate(zone, updatetype)
    dsp.conq.onConquestUpdate(zone, updatetype)
end;

function onRegionEnter(player,region)
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;
