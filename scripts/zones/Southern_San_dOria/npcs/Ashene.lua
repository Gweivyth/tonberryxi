-----------------------------------
-- Area: Southern San d'Oria
--  NPC: Ashene
-- Standard Merchant NPC
-- !pos 70 0 61 230
-----------------------------------
local ID = require("scripts/zones/Southern_San_dOria/IDs")
require("scripts/globals/npc_util")
require("scripts/globals/quests")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
    if player:getQuestStatus(SANDORIA, FLYERS_FOR_REGINE) == QUEST_ACCEPTED and npcUtil.tradeHas(trade, 532) then
        player:messageSpecial(ID.text.FLYER_REFUSED)
    end
end

function onTrigger(player,npc)
    local stock =
    {
        16455,  1888, 1,    -- Baselard 18
        16532,  5189, 1,    -- Gladius 27
        16545,  6060, 1,    -- Broadsword 30
        16576,  9791, 1,    -- Hunting Sword 34
        16524,  5051, 1,    -- Fleuret 30
        16450,   780, 2,    -- Dagger 12
        16536,  2350, 2,    -- Iron Sword 18
        16566,  2711, 2,    -- Longsword 18
        16448,    58, 3,    -- Bronze Dagger 1
        16449,   537, 3,    -- Brass Dagger 9
        16531,  1414, 3,    -- Brass Xiphos 13
        16535,    89, 3,    -- Bronze Sword 1
        16565,   777, 3,    -- Spatha 9
    }

    player:showText(npc, ID.text.ASH_THADI_ENE_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.SANDORIA)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
