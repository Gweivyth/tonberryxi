-----------------------------------
-- Area: Southern San d'Oria
--  NPC: Capucine
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Southern_San_dOria/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12473,  824,    -- Poet's Circlet
        12608,  662,    -- Tunic
        12601, 1364,    -- Linen Robe
        12736,  301,    -- Mitts
        12729,  872,    -- Linen Cuffs
        12864,  459,    -- Slacks
        12857, 1116,    -- Linen Slops
        12992,  257,    -- Solea
        12985,  725,    -- Holly Clogs
		16606,  230,    -- rusty gsword
		16655,  720,    -- rusty pick
		14117,  150,    -- rusty leggings
		14242,  170     -- rusty subligar
    }

    player:showText(npc, ID.text.CAPUCINE_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
