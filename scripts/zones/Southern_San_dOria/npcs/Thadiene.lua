-----------------------------------
-- Area: Southern San d'Oria
--  NPC: Thadiene
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Southern_San_dOria/IDs")
require("scripts/globals/npc_util")
require("scripts/globals/quests")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
    if player:getQuestStatus(SANDORIA, FLYERS_FOR_REGINE) == QUEST_ACCEPTED and npcUtil.tradeHas(trade, 532) then
        player:messageSpecial(ID.text.FLYER_REFUSED)
    end
end

function onTrigger(player,npc)
    local stock =
    {
        17280,   668, 1,    -- Boomerang 14
        17162,  7852, 1,    -- Great Bow 30
        17321,    16, 1,    -- Silver Arrow
        17154,  3560, 1,    -- Wrapped Bow 24
        17336,     5, 2,    -- Crossbow Bolt
        17322,   126, 2,    -- Fire Arrow
        17320,     7, 2,    -- Iron Arrow
        17153,   382, 2,    -- Self Bow 7
        17160,   310, 3,    -- Longbow 5
        17152,    38, 3,    -- Shortbow
        17318,     3, 3,    -- Wooden Arrow
        5029,   3755, 3,    -- Scroll of Battlefield Elegy REDUCED
    }

    player:showText(npc, ID.text.ASH_THADI_ENE_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.SANDORIA)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
