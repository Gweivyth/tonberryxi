-----------------------------------
-- Area: Southern San d'Oria
--  NPC: Carautia
-- Standard Merchant NPC
-- !pos 70 0 39 230
-----------------------------------
local ID = require("scripts/zones/Southern_San_dOria/IDs")
require("scripts/globals/npc_util")
require("scripts/globals/quests")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
    if player:getQuestStatus(SANDORIA, FLYERS_FOR_REGINE) == QUEST_ACCEPTED and npcUtil.tradeHas(trade, 532) then
        player:messageSpecial(ID.text.FLYER_REFUSED)
    end
end

function onTrigger(player,npc)
    local stock =
    {
        12808,  3970, 1,    -- Chain Hose 24
        12936,  2438, 1,    -- Greaves 24
        12306,  3340, 1,    -- Kite Shield 28
        12292,  1760, 2,    -- Mahogany Shield 20
        12826,  4920, 2,    -- Studded Trousers 30
        12954,  3016, 2,    -- Studded Boots 30
        12290,   386, 3,    -- Maple Shield 1
        12832,   137, 3,    -- Bronze Subligar 1
        12833,   820, 3,    -- Brass Subligar 11
        12824,   289, 3,    -- Leather Trousers 7
        12960,    72, 3,    -- Bronze Leggings 1
        12961,   455, 3,    -- Brass Leggings 11
        12952,   185, 3,    -- Leather Highboots 7
    }

    player:showText(npc, ID.text.CARAUTIA_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.SANDORIA)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
