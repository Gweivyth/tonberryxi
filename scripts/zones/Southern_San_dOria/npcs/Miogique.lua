-----------------------------------
-- Area: Southern San d'Oria
--  NPC: Miogique
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Southern_San_dOria/IDs")
require("scripts/globals/npc_util")
require("scripts/globals/quests")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
    if player:getQuestStatus(SANDORIA, FLYERS_FOR_REGINE) == QUEST_ACCEPTED and npcUtil.tradeHas(trade, 532) then
        player:messageSpecial(ID.text.FLYER_REFUSED)
    end
end

function onTrigger(player,npc)
    local stock =
    {
        12552,  4474, 1,    -- Chainmail 24
        12680,  2890, 1,    -- Chain Mittens 24
        12672,  5892, 1,    -- Gauntlets 40
        12424,  3303, 1,    -- Iron Mask 24
        12442,  3474, 2,    -- Studded Bandana 30
        12698,  3131, 2,    -- Studded Gloves 30
        12570,  6088, 2,    -- Studded Vest 30
        12449,   653, 3,    -- Brass Cap
        12577,  1078, 3,    -- Brass Harness 11
        12705,   566, 3,    -- Brass Mittens 11
        12448,   121, 3,    -- Bronze Cap 1
        12576,   366, 3,    -- Bronze Harness 1
        12704,    78, 3,    -- Bronze Mittens 1
        12440,   202, 3,    -- Leather Bandana 7
        12696,   194, 3,    -- Leather Gloves 7
        12568,   362, 3,    -- Leather Vest 7
    }

    player:showText(npc, ID.text.MIOGIQUE_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.SANDORIA)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
