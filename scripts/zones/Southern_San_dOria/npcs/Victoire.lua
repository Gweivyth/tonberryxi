-----------------------------------
-- Area: Southern San d'Oria
--  NPC: Victoire
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Southern_San_dOria/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12432,  724,    -- Faceguard 10
        12464,  842,    -- Headgear
        12560, 1115,    -- Scale Mail 10
        12592, 1110,    -- Doublet
        12688,  536,    -- Scale Fng. Gnt. 10
        12720,  607,    -- Gloves
        12816,  783,    -- Scale Cuisses 10
        12848,  940,    -- Brais
        12944,  445,    -- Scale Greaves 10
        12976,  635,    -- Gaiters
    }

    player:showText(npc, ID.text.CARAUTIA_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
