-----------------------------------
-- Area: Northern San d'Oria
--  NPC: Arlenne
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Northern_San_dOria/IDs")
require("scripts/globals/npc_util")
require("scripts/globals/quests")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
    if player:getQuestStatus(SANDORIA,FLYERS_FOR_REGINE) == QUEST_ACCEPTED and npcUtil.tradeHas(trade, 532) then
        player:messageSpecial(ID.text.FLYER_REFUSED)
    end
end

function onTrigger(player,npc)
    local stock =
    {
        17051,   623, 1,    -- Yew Wand 18
        17090,  1384, 1,    -- Elm Staff 23
        17097,  4901, 1,    -- Elm Pole 30
        16770,  4471, 1,    -- Zaghnal 20
        17096,  2059, 2,    -- Holly Pole 16
        17024,    66, 3,    -- Ash Club 1
        17049,    46, 3,    -- Maple Wand 1
        17050,   340, 3,    -- Willow Wand 9
        17088,    57, 3,    -- Ash Staff 1
        17089,   484, 3,    -- Holly Staff 11
        17095,   325, 3,    -- Ash Pole 5
        16385,    70, 3,    -- Cesti 1
        16391,   855, 3,    -- Brass Knuckles 9
        16407,   912, 3,    -- Brass Baghnakhs 11
        16768,   199, 3,    -- Bronze Zaghnal 1
        16769,  1807, 2,    -- Brass Zaghnal 10
    }

    player:showText(npc,ID.text.ARLENNE_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.SANDORIA)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
