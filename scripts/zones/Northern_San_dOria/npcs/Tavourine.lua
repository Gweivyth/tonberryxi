-----------------------------------
-- Area: Northern San d'Oria
--  NPC: Tavourine
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Northern_San_dOria/IDs")
require("scripts/globals/npc_util")
require("scripts/globals/quests")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
    if player:getQuestStatus(SANDORIA, FLYERS_FOR_REGINE) == QUEST_ACCEPTED and npcUtil.tradeHas(trade, 532) then
        player:messageSpecial(ID.text.FLYER_REFUSED)
    end
end

function onTrigger(player,npc)
    local stock =
    {
        16584, 10584, 1,    -- Mythril Claymore 36
        16466,   755, 1,    -- Knife 13
        17060,   961, 1,    -- Rod 22
        16640,   177, 2,    -- Bronze Axe 1
        16465,    90, 2,    -- Bronze Knife 1
		16833,   609, 3,    -- Bronze Spear 7
        17081,   333, 2,    -- Brass Rod 12
        16583,  1213, 2,    -- Claymore 10
        17035,  1466, 2,    -- Mace 19
        17059,    60, 3,    -- Bronze Rod 5
        17034,    94, 3,    -- Bronze Mace 4
        16845,  5581, 3,    -- Lance 34
    }

    player:showText(npc,ID.text.TAVOURINE_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.SANDORIA)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
