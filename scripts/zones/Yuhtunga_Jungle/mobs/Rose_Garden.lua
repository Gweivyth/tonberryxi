-----------------------------------
-- Area: Yuhtunga Jungle
--  MOB: Rose Garden
-----------------------------------
local ID = require("scripts/zones/Yuhtunga_Jungle/IDs")

function onMobSpawn(mob)
    local var = math.random(36000,37800) + GetServerVariable("roseoverflow")
	mob:setLocalVar("timeToGrow", var) -- 10:00:00 to 10:30:00
end

function onMobDisengage(mob)
    mob:setLocalVar("timeToGrow", os.time() + math.random(36000,37800)) -- 10:00:00 to 10:30:00
	SetServerVariable("roseoverflow", os.time())
end

function onMobRoam(mob)
    -- Rose Garden has been left alone for 10.25 hours
    if os.time() >= mob:getLocalVar("timeToGrow") then
        DisallowRespawn(ID.mob.ROSE_GARDEN, true)
        DespawnMob(ID.mob.ROSE_GARDEN)
        DisallowRespawn(ID.mob.VOLUPTUOUS_VILMA, false)
        pos = mob:getPos()
        SpawnMob(ID.mob.VOLUPTUOUS_VILMA):setPos(pos.x, pos.y, pos.z, pos.rot)
    end
end

function onMobDeath(mob, player, isKiller)
	local rando = math.random(36000,37800)
    GetMobByID(ID.mob.ROSE_GARDEN_PH):setLocalVar("timeToGrow", os.time() + rando) -- 10:00:00 to 10:30:00
	SetServerVariable("roseoverflow", os.time() + rando)
end

function onMobDespawn(mob)
    if os.time() < mob:getLocalVar("timeToGrow") then
        DisallowRespawn(ID.mob.ROSE_GARDEN, true)
        DisallowRespawn(ID.mob.ROSE_GARDEN_PH, false)
        GetMobByID(ID.mob.ROSE_GARDEN_PH):setRespawnTime(GetMobRespawnTime(ID.mob.ROSE_GARDEN_PH))
    end
end
