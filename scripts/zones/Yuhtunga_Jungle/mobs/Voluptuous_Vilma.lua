-----------------------------------
-- Area: Yuhtunga Jungle
--  MOB: Voluptuous Vilma
-----------------------------------
local ID = require("scripts/zones/Yuhtunga_Jungle/IDs")
-----------------------------------

function onMobDeath(mob, player, isKiller)
	local rando = math.random(36000,37800)
    GetMobByID(ID.mob.ROSE_GARDEN_PH):setLocalVar("timeToGrow", os.time() + rando) -- 10:00:00 to 10:30:00
	SetServerVariable("roseoverflow", os.time() + rando)
end

function onMobDespawn(mob)
    DisallowRespawn(ID.mob.VOLUPTUOUS_VILMA, true)
    DisallowRespawn(ID.mob.ROSE_GARDEN_PH, false)
    GetMobByID(ID.mob.ROSE_GARDEN_PH):setRespawnTime(GetMobRespawnTime(ID.mob.ROSE_GARDEN_PH))
end
