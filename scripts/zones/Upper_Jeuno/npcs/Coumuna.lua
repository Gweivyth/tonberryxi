-----------------------------------
-- Area: Upper Jeuno
--  NPC: Coumuna
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Upper_Jeuno/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        16705,  2069,    -- Greataxe 12
        16518,  8680,    -- Mythril Degen 36
        16460,  3951,    -- Kris 27
        16467,  4268,    -- Mythril Knife 31
        16399,  4748,    -- Katars 33
        16589,  5784,    -- Two-Handed Sword 20
        16412,  7440,    -- Mythril Claws 41
        16567, 19607,    -- Knight's Sword 47
        16644, 12636,    -- Mythril Axe 37
        17061,  3628,    -- Mythril Rod 34
        17027,  3246,    -- Oak Cudgel 36
        17036,  5053,    -- Mythril Mace ?
        17044,  3031,    -- Warhammer 20
        17098,  9360,    -- Oak Pole 40
        16836, 11132,    -- Halberd 36
        16774,  5298,    -- Scythe 18
        17320,     8,    -- Iron Arrow
    }

    player:showText(npc, ID.text.COUMUNA_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
