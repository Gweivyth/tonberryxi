-----------------------------------
-- Area: Upper Jeuno
--  NPC: Khe Chalahko
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Upper_Jeuno/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12416,  6788,    -- Sallet
        12544, 10890,    -- Breastplate
        12800,  8749,    -- Cuisses
		12672,  5995,    -- Gauntlets 40
        12928,  6258,    -- Plate Leggins
        12810, 13138,    -- Breeches
        12938,  8095,    -- Sollerets
    }

    player:showText(npc, ID.text.KHECHALAHKO_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
