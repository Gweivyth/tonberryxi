-----------------------------------
-- Area: Upper Jeuno
--  NPC: Deadly Minnow
-- Standard Merchant NPC
-- Involved in Quest: Borghertz's Hands (1st quest only)
-- !pos -5 1 48 244
-----------------------------------
local ID = require("scripts/zones/Upper_Jeuno/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)

    if player:getVar("BorghertzHandsFirstTime") == 1 then
        player:startEvent(24)
        player:setVar("BorghertzHandsFirstTime", 2)
    else
        local stock =
        {
            12442, 3274,    -- Studded Bandana 30
            12425, 6384,    -- Silver Mask 36
            12426, 10915,    -- Banded Helm 46
            12570, 5781,    -- Studded Vest 30
            12553, 9856,    -- Silver Mail 36
            12554, 16516,    -- Banded Mail 46
            12698, 2890,    -- Studded Gloves 30
            12681, 5264,    -- Silver Mittens 36
            12682, 8821,    -- Mufflers 46
        }

        player:showText(npc, ID.text.DEADLYMINNOW_SHOP_DIALOG)
        dsp.shop.general(player, stock)
    end
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
