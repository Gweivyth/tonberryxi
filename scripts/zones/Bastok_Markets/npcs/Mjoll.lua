-----------------------------------
-- Area: Batok Markets
--  NPC: Mjoll
-- Standard Merchant NPC
-- !pos -318.902 -10.319 -178.087 235
-----------------------------------
local ID = require("scripts/zones/Bastok_Markets/IDs")
require("scripts/globals/shop")

function onTrigger(player,npc)
    local stock =
    {
        17321,    16, 3, -- Silver Arrow
        17218, 14158, 3, -- Zamburak
        17298,   294, 3, -- Tathlum
        17318,     3, 3, -- Wooden Arrow
        17337,    22, 3, -- Mythril Bolt
        17320,     7, 3, -- Iron Arrow
        5069,    199, 3, -- Scroll of Dark Threnody
        5063,   1000, 3, -- Scroll of Ice Threnody
        17216,   165, 3, -- Light Crossbow
        17217,  2166, 3, -- Crossbow
        17336,     5, 3, -- Crossbow Bolt
		16606,   220, 3, -- rusty greatsword
		16655,   720, 3, -- rusty pick
		14117,   150, 3, -- rusty leggings
		14242,   170, 3  -- rusty subligar
    }

    player:showText(npc, ID.text.MJOLL_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.BASTOK)
end
