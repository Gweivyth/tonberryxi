-----------------------------------
-- Area: Bastok Markets
--  NPC: Brunhilde
-- Standard Merchant NPC
-- !pos -305.775 -10.319 -152.173 235
-----------------------------------
local ID = require("scripts/zones/Bastok_Markets/IDs")
require("scripts/globals/shop")

function onTrigger(player,npc)
    local stock =
    {
        12448,   124, 3,    -- Bronze Cap 1
        12432,   734, 3,    -- Faceguard 10
        12433,  4876, 2,    -- Brass Mask 27
        12416,  8311, 2,    -- Sallet 40
        12576,   215, 3,    -- Bronze Harness 1
        12560,  1111, 3,    -- Scale Mail 10
        12561,  6168, 2,    -- Brass Scale Mail 27
        12704,   118, 3,    -- Bronze Mittens 1
        12688,   524, 3,    -- Scale Finger Gauntlets 10
        12689,  3189, 2,    -- Brass Finger Gauntlets 27
        12417, 10857, 1,    -- Mythril Sallet 49
        12544, 11302, 1,    -- Breastplate 40
        12672,  5761, 1     -- Gauntlets 40
    }

    player:showText(npc, ID.text.BRUNHILDE_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.BASTOK)
end


-- 10 // 0.5
-- 20 // 0.4
-- 30 // 0.3
-- 40 // 0.25
-- 50 // 0.2