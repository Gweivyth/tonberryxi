-----------------------------------
-- Area: Port Windurst
--  NPC: Aroro
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Port_Windurst/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        4862,  114, 1,    -- Blind
        4828,   84, 2,    -- Poison
        4838,  312, 2,    -- Bio
        4861, 2080, 2,    -- Sleep
        4767,   62, 3,    -- Stone
        4777,  143, 3,    -- Water
        4762,  304, 3,    -- Aero
        4752,  789, 3,    -- Fire
        4757, 1456, 3,    -- Blizzard
        4772, 2905, 3,    -- Thunder
        4843, 4222, 3,    -- Burn
        4844, 3490, 3,    -- Frost
        4845, 2184, 3,    -- Choke
        4846, 1683, 3,    -- Rasp
        4847, 1225, 3,    -- Shock
        4848, 6190, 3,    -- Drown
    }

    player:showText(npc, ID.text.ARORO_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.WINDURST)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
