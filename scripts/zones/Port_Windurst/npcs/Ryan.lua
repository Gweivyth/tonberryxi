-----------------------------------
-- Area: Port Windurst
--  NPC: Ryan
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Port_Windurst/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        16640,  220,    -- Bronze Axe
        16535,  176,    -- Bronze Sword
        17336,    5,    -- Crossbow Bolt
        12576,  175,    -- Bronze Harness
        12577, 1286,    -- Brass Harness
        12704,  108,    -- Bronze Mittens
        12705,  655,    -- Brass Mittens
        12832,  151,    -- Bronze Subligar
        12833,  840,    -- Brass Subligar
        12960,   87,    -- Bronze Leggings
        12961,  540,    -- Brass Leggings
        12584,  677,    -- Kenpogi
        12712,  350,    -- Tekko
        12840,  551,    -- Sitabaki
        12968,  299     -- Kyahan
    }

    player:showText(npc, ID.text.RYAN_SHOP_DIALOG)
    dsp.shop.general(player, stock, WINDURST)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
