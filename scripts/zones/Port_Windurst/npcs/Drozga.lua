-----------------------------------
-- Area: Port Windurst
--  NPC: Drozga
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Port_Windurst/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12432,  724,    -- Faceguard
        12560, 1115,    -- Scale Mail
        12688,  536,    -- Scale Fng. Gnt.
        12816,  901,    -- Scale Cuisses
        12944, 550,    -- Scale Greaves
        13192,  230,    -- Leather Belt
        13327,  785,    -- Silver Earring
        13469,  544,    -- Leather Ring
		483,   3251     -- bkn mithran rod for opo crown
    }

    player:showText(npc,ID.text.DROZGA_SHOP_DIALOG)
    dsp.shop.general(player, stock, WINDURST)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end

-- 10 // 0.5
-- 20 // 0.4
-- 30 // 0.3