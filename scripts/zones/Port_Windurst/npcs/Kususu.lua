-----------------------------------
-- Area: Port Windurst
--  NPC: Kususu
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Port_Windurst/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        4641,  988, 1,    -- Diaga
        4662, 6025, 1,    -- Stoneskin
        4664,  737, 1,    -- Slow
        4610,  522, 2,    -- Cure II
        4636,  140, 2,    -- Banish
        4646, 1012, 2,    -- Banishga
        4661, 1875, 2,    -- Blink
        4609,   61, 3,    -- Cure
        4615, 1213, 3,    -- Curaga
        4622,  140, 3,    -- Poisona
        4623,  268, 3,    -- Paralyna
        4624,  780, 3,    -- Blindna
        4631,   82, 3,    -- Dia
        4651,  219, 3,    -- Protect
        4656, 1371, 3,    -- Shell
        4663,  272, 3,    -- Aquaveil
    }

    player:showText(npc, ID.text.KUSUSU_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.WINDURST)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
