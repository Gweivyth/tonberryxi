-----------------------------------
-- Area: Port Windurst
--  NPC: Kumama
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Port_Windurst/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12849, 3157, 2,    -- Cotton Brais
        12977, 2224, 2,    -- Cotton Gaiters
        12848,  940, 3,    -- Brais
        12856,  172, 3,    -- Slops
        12824,  289, 3,    -- Leather Trousers
        12864,  474, 3,		-- Slacks
        12857, 1080, 3,    -- Linen Slops
        12976,  635, 3,    -- Gaiters
        12984,  111, 3,    -- Ash Clogs
        12952,  185, 3,    -- Leather Highboots
        12992,  312, 3,    -- Solea
        12985,  704, 3,    -- Holly Clogs
        12289,  60, 3,    -- Lauan Shield
        12290,  386, 3,    -- Maple Shield
    }

    player:showText(npc, ID.text.KUMAMA_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.WINDURST)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
