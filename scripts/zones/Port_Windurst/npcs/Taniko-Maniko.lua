-----------------------------------
-- Area: Port Windurst
--  NPC: Taniko-Maniko
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Port_Windurst/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        16649, 2041, 2,    -- Bone Pick
        16405,  104, 3,    -- Cat Baghnakhs
        16385,  129, 3,    -- Cesti
        16391,  855, 3,    -- Brass Knuckles
        16407,  912, 3,    -- Brass Baghnakhs
        16642, 1888, 3,    -- Bone Axe
        16768,  199, 3,    -- Bronze Zaghnal
        16769, 1807, 3,    -- Brass Zaghnal
        16832,   77, 3,    -- Harpoon
        16448,   58, 3,    -- Bronze Dagger
        16449,  537, 3,    -- Brass Dagger
        16450,  780, 3,    -- Dagger
        16512, 1076, 3,    -- Bilbo
        16530,  418, 3,    -- Xiphos
        16565,  777, 3,    -- Spatha
    }

    player:showText(npc, ID.text.TANIKOMANIKO_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.WINDURST)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
