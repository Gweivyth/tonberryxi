-----------------------------------
-- Area: Rabao
--  NPC: Shiny Teeth
-- Standard Merchant NPC
-- !pos -30 8 99 247
-----------------------------------
local ID = require("scripts/zones/Rabao/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        16450,    780,    -- Dagger 12
        16460,   3657,    -- Kris 27
        16466,    755,    -- Knife 13
        16552,   2021,    -- Scimitar 13
        16553,   8827,    -- Tulwar 36
        16558,  13640,    -- Falchion 44
        17060,    961,    -- Rod 22
        16401,  22874,    -- Jamadhars 61
        17155,   6926,    -- Composite Bow 36
        17298,    294,    -- Tathlum
        17320,      7,    -- Iron Arrow
        17340,     92,    -- Bullet
        17315,   5460,    -- Riot Grenade
        17284,   3167,    -- Chakram 28
		
    }

    player:showText(npc, ID.text.SHINY_TEETH_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end