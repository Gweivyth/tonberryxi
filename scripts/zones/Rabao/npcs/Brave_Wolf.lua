-----------------------------------
-- Area: Rabao
--  NPC: Brave Wolf
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Rabao/IDs");
require("scripts/globals/shop");

function onTrade(player,npc,trade)
end;

function onTrigger(player,npc)
    local stock =
    {
        12301,  7749,    -- Buckler 45
        12302, 13065,    -- Darksteel Buckler 59
        13979,  6062,    -- Silver Bangles 41
        12554, 16516,    -- Banded Mail 46
        12682,  8821,    -- Mufflers 46
        12810, 13138,    -- Breeches 46
        12938,  8095,    -- Sollerets 46
        12609,  3769,    -- Black Tunic 20
        12737,  1712,    -- White Mitts 20
        12865,  2411,    -- Black Slacks 20
        12993,  1233,    -- Sandals 20
        12578,  7163,    -- Padded Armor 35
        12706,  3931,    -- Iron Mittens 35
        12836,  5765,    -- Iron Subligar 35
        12962,  3481,    -- Leggings 35
		717, 13450, -- mahogany lumber
		654, 27900, -- dsteel ingot
		12494, 32520, -- gold hairpin
		13315, 39760, -- gold earring
    }

    player:showText(npc, ID.text.BRAVEWOLF_SHOP_DIALOG)
    dsp.shop.general(player, stock);
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;
