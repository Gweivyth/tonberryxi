-----------------------------------
-- Area: Mount Zhayolm
--  NPC: ??? (Spawn Sarameya(ZNM T4))
-- !pos 322 -14 -581 61
-----------------------------------
local ID = require("scripts/zones/Mount_Zhayolm/IDs");
require("scripts/globals/npc_util");
-----------------------------------

function onTrade(player,npc,trade)
end;

function onTrigger(player,npc)

	local PuppetmasterBluesProgress = player:getVar("PuppetmasterBluesProgress")
	
	if PuppetmasterBluesProgress == 2 then
		player:setVar("PuppetmasterBluesProgress",3)
		player:addKeyItem(844)
	else
		player:messageSpecial(ID.text.NOTHING_HAPPENS)
	end
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;
