-----------------------------------
--
-- Zone: Valley_of_Sorrows (128)
--
-----------------------------------
local ID = require("scripts/zones/Valley_of_Sorrows/IDs");
require("scripts/globals/conquest");
require("scripts/globals/settings");
require("scripts/globals/zone");
-----------------------------------

function onInitialize(zone)
    if (LandKingSystem_NQ ~= 1) then
        UpdateNMSpawnPoint(ID.mob.ADAMANTOISE);
		UpdateNMSpawnPoint(ID.mob.ASPIDOCHELONE)
        --GetMobByID(ID.mob.FAFNIR):setRespawnTime(math.random(900, 10800));
		
		local re = GetServerVariable("AdamantoiseRespawn")
		if os.time() < re then
			if GetServerVariable("AdamantoiseSwitch") == 0 then
				GetMobByID(ID.mob.ADAMANTOISE):setRespawnTime(re - os.time())
			else
				DisallowRespawn(ID.mob.ASPIDOCHELONE, false)
				DisallowRespawn(ID.mob.ADAMANTOISE, true)
				GetMobByID(ID.mob.ASPIDOCHELONE):setRespawnTime(re - os.time())
			end
		else
			if GetServerVariable("AdamantoiseSwitch") == 0 then
				SpawnMob(ID.mob.ADAMANTOISE)
			else
				DisallowRespawn(ID.mob.ASPIDOCHELONE, false)
				DisallowRespawn(ID.mob.ADAMANTOISE, true)
				SpawnMob(ID.mob.ASPIDOCHELONE)
			end
		end
    end
end;

function onConquestUpdate(zone, updatetype)
    dsp.conq.onConquestUpdate(zone, updatetype)
end;

function onZoneIn(player,prevZone)
    local cs = -1;

    if (player:getXPos() == 0 and player:getYPos() == 0 and player:getZPos() == 0) then
        player:setPos(45.25,-2.115,-140.562,0);
    end

    return cs;
end;

function onRegionEnter(player,region)
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;
