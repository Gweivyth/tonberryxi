-----------------------------------
-- Area: Kazham
--  NPC: Tahn Posbei
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Kazham/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        12289,    90,    -- Lauan Shield 1
        12292,  2180,    -- Mahogany Shield 20
        12295, 15965,    -- Round Shield 61
        12455,  2810,    -- Beetle Mask
        12583,  4332,    -- Beetle Harness
        12711,  2282,    -- Beetle Mittens
        12835,  3466,    -- Beetle Subligar
        12967,  2132,    -- Beetre Leggins
        12440,   202,    -- Leather Bandana
        12568,   362,    -- Leather Vest
        12696,   194,    -- Leather Gloves
        12952,   185,    -- Leather Highboots
        13092,  6777,    -- Coeurl Gorget 61
    }

    player:showText(npc, ID.text.TAHNPOSBEI_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
