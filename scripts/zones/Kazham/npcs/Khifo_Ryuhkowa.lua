-----------------------------------
-- Area: Kazham
--  NPC: Khifo Ryuhkowa
-- Standard Merchant NPC
-----------------------------------
local ID = require("scripts/zones/Kazham/IDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        16473,   1890,    -- Kukri 20
        16595,  35662,    -- Ram-Dao 59
        16833,    609,    -- Bronze Spear 7
        16835,   5890,    -- Spear 24
        16839,  21885,    -- Partisan 57
        17025,    850,    -- Chestnut Club 16
        17026,   1973,    -- Bone Cudgel 27
        17052,   2200,    -- Chestnut Wand 32
        17092,  23752,    -- Mahogany Staff 59
        17099,  29800,    -- Mahogany Pole 59
        17163,  15897,    -- Battle Bow 40
        17308,     55,    -- Hawkeye
        17280,    668,    -- Boomerang 14
        17318,      3,    -- Woden Arrow
    }

    player:showText(npc, ID.text.KHIFORYUHKOWA_SHOP_DIALOG)
    dsp.shop.general(player, stock)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
