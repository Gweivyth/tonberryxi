require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/msg")

-- ecosystem
ECO_BEAST = 1
ECO_LIZARD = 2
ECO_VERMIN = 3
ECO_PLANTOID = 4

ECO_AQUAN = 5
ECO_AMORPH = 6
ECO_BIRD = 7

ECO_UNDEAD = 8
ECO_ARCANA = 9

ECO_DRAGON = 10
ECO_DEMON = 11

ECO_LUMORIAN = 12
ECO_LUMINION = 13

-- The type of spell.
SPELLTYPE_PHYSICAL = 0;
SPELLTYPE_MAGICAL = 1;
SPELLTYPE_RANGED = 2;
SPELLTYPE_BREATH = 3;
SPELLTYPE_DRAIN = 4;
SPELLTYPE_SPECIAL = 5;

-- The TP modifier
TPMOD_NONE = 0;
TPMOD_CRITICAL = 1;
TPMOD_DAMAGE = 2;
TPMOD_ACC = 3;
TPMOD_ATTACK = 4;

-- The damage type for the spell
DMGTYPE_BLUNT = 0;
DMGTYPE_PIERCE = 1;
DMGTYPE_SLASH = 2;
DMGTYPE_H2H = 3;

-- The SC the spell makes
SC_IMPACTION = 0;
SC_TRANSFIXION = 1;
SC_DETONATION = 2;
SC_REVERBERATION = 3;
SC_SCISSION = 4;
SC_INDURATION = 5;
SC_LIQUEFACTION = 6;
SC_COMPRESSION = 7;

SC_FUSION = 8;
SC_FRAGMENTATION = 9;
SC_DISTORTION = 10;
SC_GRAVITATION = 11;

SC_DARK = 12;
SC_LIGHT = 13;

INT_BASED = 1;
CHR_BASED = 2;
MND_BASED = 3;

-- Get the damage for a blue magic physical spell.
-- caster - The entity casting the spell.
-- target - The target of the spell.
-- spell - The blue magic spell itself.
-- params - The parameters for the spell. Broken down into:
--      .tpmod - The TP modifier for the spell (e.g. damage varies, critical varies with TP, etc). Should be a TPMOD_xxx enum.
--      .numHits - The number of hits in the spell.
--      .multiplier - The base multiplier for the spell (not under Chain Affinity) - Every spell must specify this. (equivalent to TP 0%)
--      .tp150 - The TP modifier @ 150% TP (damage multiplier, crit chance, etc. 1.0 = 100%, 2.0 = 200% NOT 100=100%).
--               This value is interpreted as crit chance or dmg multiplier depending on the TP modifier (tpmod).
--      .tp300 - The TP modifier @ 300% TP (damage multiplier, crit chance, etc. 1.0 = 100%, 2.0 = 200% NOT 100=100%)
--               This value is interpreted as crit chance or dmg multiplier depending on the TP modifier (tpmod).
--      .azuretp - The TP modifier under Azure Lore (damage multiplier, crit chance, etc. 1.0 = 100%, 2.0 = 200% NOT 100=100%)
--                  This value is interpreted as crit chance or dmg multiplier depending on the TP modifier (tpmod).
--      .duppercap - The upper cap for D for this spell.
--      .str_wsc - The decimal % value for STR % (e.g. STR 20% becomes 0.2)
--      .dex_wsc - Same as above.
--      .vit_wsc - Same as above.
--      .int_wsc - Same as above.
--      .mnd_wsc - Same as above.
--      .chr_wsc - Same as above.
--      .agi_wsc - Same as above.

function GetTargetEcosystem(target)
	local sys = target:getSystem()
	
	if sys == 6 then return 1
	elseif sys == 14 then return 2
	elseif sys == 20 then return 3
	elseif sys == 17 then return 4
	elseif sys == 2 then return 5
	elseif sys == 1 then return 6
	elseif sys == 8 then return 7
	elseif sys == 19 then return 8
	elseif sys == 3 then return 9
	elseif sys == 10 then return 10
	elseif sys == 9 then return 11
	elseif sys == 15 then return 12
	elseif sys == 16 then return 13 end
	
	return 0
	
end

function GetMonsterCorrelationMult(eco,targeco)

	if eco == 1 then
		if targeco == 2 then return 1.25 end
		if targeco == 4 then return 0.8 end
	elseif eco == 2 then
		if targeco == 3 then return 1.25 end
		if targeco == 1 then return 0.8 end
	elseif eco == 3 then
		if targeco == 4 then return 1.25 end
		if targeco == 2 then return 0.8 end
	elseif eco == 4 then
		if targeco == 1 then return 1.25 end
		if targeco == 3 then return 0.8 end
	elseif eco == 5 then
		if targeco == 6 then return 1.25 end
		if targeco == 7 then return 0.8 end
	elseif eco == 6 then
		if targeco == 7 then return 1.25 end
		if targeco == 5 then return 0.8 end
	elseif eco == 7 then
		if targeco == 5 then return 1.25 end
		if targeco == 6 then return 0.8 end
	elseif eco == 8 and targeco == 9 then
		return 1.25
	elseif eco == 9 and targeco == 8 then
		return 1.25
	elseif eco == 10 and targeco == 11 then
		return 1.25
	elseif eco == 11 and targeco == 10 then
		return 1.25
	elseif eco == 12 and targeco == 13 then
		return 1.25
	elseif eco == 13 and targeco == 12 then
		return 1.25
	end
	
	return 1.0
	
end

function BluePhysicalSpell(caster, target, spell, params)
    -- store related values
    local magicskill = caster:getSkillLevel(dsp.skill.BLUE_MAGIC); -- skill + merits + equip bonuses
    -- TODO: Under Chain affinity?
    -- TODO: Under Efflux?
    -- TODO: Merits.
    -- TODO: Under Azure Lore.
	
	--SATA.
	local SAmult = 1
	local SAbonus = 0
	local TAbonus = 0
	local taChar = nil
	if params.SA ~= nil then
		local isSneakValid = caster:hasStatusEffect(dsp.effect.SNEAK_ATTACK)
		if (isSneakValid and not (caster:isBehind(target) or caster:hasStatusEffect(dsp.effect.HIDE) or caster:hasStatusEffect(dsp.effect.DOUBT))) then
			isSneakValid = false
		end
		if isSneakValid then
			SAmult = params.SA
			SAbonus = caster:getStat(dsp.mod.DEX) * (1 + caster:getMod(dsp.mod.SNEAK_ATK_DEX)/100) * (100+(caster:getMod(dsp.mod.AUGMENTS_SA)))/100
			SAbonus = math.max(SAbonus - 10, 1)
		end
		taChar = caster:getTrickAttackChar(target)
		if taChar ~= nil then
			TAbonus = caster:getStat(dsp.mod.AGI) * (1 + caster:getMod(dsp.mod.TRICK_ATK_AGI)/100) * (100+(caster:getMod(dsp.mod.AUGMENTS_TA)))/100
			TAbonus = math.max(TAbonus - 10, 1)
		end
		caster:delStatusEffect(dsp.effect.SNEAK_ATTACK)
		caster:delStatusEffect(dsp.effect.TRICK_ATTACK)
	end
	--print(string.format("SAmult = %f , SAbonus = %i , TAbonus = %i",SAmult,SAbonus,TAbonus))

    ---------------------------------
    -- Calculate the final D value  -
    ---------------------------------
    -- worked out from http://wiki.ffxiclopedia.org/wiki/Calculating_Blue_Magic_Damage
    -- Final D value ??= floor(D+fSTR+WSC) * Multiplier

    local D =  math.floor((magicskill * 0.11)) * 2 + 3;
    -- cap D
    if (D > params.duppercap) then
        D = params.duppercap;
    end

    -- print("D val is ".. D);
	
	--print(string.format("casterSTR = %i",caster:getStat(dsp.mod.STR)))
	--print(string.format("targetVIT = %i",target:getStat(dsp.mod.VIT)))
	--print(string.format("dSTR = %i",caster:getStat(dsp.mod.STR)-target:getStat(dsp.mod.VIT)))
	
    local fStr = BluefSTR(caster:getStat(dsp.mod.STR) - target:getStat(dsp.mod.VIT));
    if (fStr > 22) then
        fStr = 22; -- TODO: Smite of Rage doesn't have this cap applied.
    end

    local WSC = BlueGetWsc(caster, params);

    -- print("wsc val is ".. WSC);

    local multiplier = params.multiplier;

    -- If under CA, replace multiplier with fTP(multiplier, tp150, tp300)
    local chainAffinity = caster:getStatusEffect(dsp.effect.CHAIN_AFFINITY);
    if chainAffinity ~= nil then
        -- Calculate the total TP available for the fTP multiplier.
        local tp = caster:getTP() + caster:getMerit(dsp.merit.ENCHAINMENT);
        if tp > 3000 then
            tp = 3000;
        end;

        multiplier = BluefTP(tp, multiplier, params.tp150, params.tp300);
    end;
	
	--print(string.format("D = %i",D))
	--print(string.format("fstr = %i",fStr))
	--print(string.format("WSC = %i",WSC))
	
    -- TODO: Modify multiplier to account for family bonus/penalty
    local finalD = math.floor(D + fStr + WSC) * multiplier;
	local cost = spell:getMPCost()
	local cap = math.floor((cost * 2.3) - 6)
	if finalD > cap then
		local softcapratio = 0.1+(cost-4)/100
		if params.numhits > 1 then
			softcapratio = softcapratio-0.1
		end
		if softcapratio > 0.75 then
			softcapratio = 0.75
		end
		finalD = cap + math.floor((finalD - cap)*softcapratio)
		--print("HIT SOFT CAP!")
	end
	
	--print(string.format("upper limit soft cap: %i",cap))
	
    -- print("Final D is ".. finalD);

    ----------------------------------------------
    -- Get the possible pDIF range and hit rate --
    ----------------------------------------------
    if (params.offcratiomod == nil) then -- default to attack. Pretty much every physical spell will use this, Cannonball being the exception.
        params.offcratiomod = caster:getStat(dsp.mod.ATT)
    end;
    -- print(params.offcratiomod)
    local cratio = BluecRatio(params.offcratiomod / target:getStat(dsp.mod.DEF), caster:getMainLvl(), target:getMainLvl());
    local hitrate = BlueGetHitRate(caster,target,true);
	
	if params.flat_dmg ~= nil then
		hitrate = hitrate * 0.9 -- 1k needles has less acc
	end

    --print("hitrate = "..hitrate);
    -- print("pdifmin "..cratio[1].." pdifmax "..cratio[2]);

    -------------------------
    -- Perform the attacks --
    -------------------------
    local hitsdone = 0;
    local hitslanded = 0;
    local finaldmg = 0;
	
	
    while (hitsdone < params.numhits) do
        local chance = math.random();
		if hitsdone > 0 then
			chance = chance + 0.12 -- -12% chance to hit on hits past the first
		end
        if (chance < hitrate or SAmult > 1) then -- it hit
            -- TODO: Check for shadow absorbs.

            -- Generate a random pDIF between min and max
            local pdif = math.random((cratio[1]*1000),(cratio[2]*1000));
            pdif = pdif/1000;

            -- Apply it to our final D
            if (hitsdone == 0) then -- only the first hit benefits from multiplier
                finaldmg = finaldmg + (finalD * pdif)
				finaldmg = finaldmg * SAmult
				finaldmg = finaldmg + SAbonus + TAbonus
            else
                finaldmg = finaldmg + ((math.floor(D + fStr + WSC)) * pdif) -- same as finalD but without multiplier (it should be 1.0)
            end

            hitslanded = hitslanded + 1

            -- increment target's TP (100TP per hit landed)
            target:addTP(100);
        end
		
		SAmult = 1
		SAbonus = 0
		TAbonus = 0
		
        hitsdone = hitsdone + 1;
    end
	
	if params.numhits > 1 then
		finaldmg = finaldmg*((1 / params.numhits)+0.33)*((params.numhits-1)*0.14+1) 	-- 2hit 0.6, 3hit 0.43, 4hit 0.35, 5hit 0.3
	end					-- post numhit correction:	--    		1.2,      1.30,      1.40		1.5
	
	if params.endmult ~= nil then
		finaldmg = finaldmg * params.endmult
	end
	
	if finaldmg < 1 then
		finaldmg = 1
	end
	
	if params.dmgtype == DMGTYPE_PIERCE then
		finaldmg = math.ceil(finaldmg * target:getMod(dsp.mod.PIERCERES) / 1000)
	elseif params.dmgtype == DMGTYPE_BLUNT or params.dmgtype == DMGTYPE_H2H then
		finaldmg = math.ceil(finaldmg * target:getMod(dsp.mod.IMPACTRES) / 1000)
	elseif params.dmgtype == DMGTYPE_SLASH then
		finaldmg = math.ceil(finaldmg * target:getMod(dsp.mod.SLASHRES) / 1000)
	end
	
	local correlmult = GetMonsterCorrelationMult(params.eco,GetTargetEcosystem(target))
	
	if correlmult > 1 then
		finaldmg = math.ceil(finaldmg * correlmult) + math.random(0,4)
	else
		finaldmg = math.floor(finaldmg * correlmult)
	end
	
	--print(string.format("correlmult = %f",correlmult))
	
    --print("Hits landed "..hitslanded.."/"..hitsdone.." for total damage: "..finaldmg);
	if hitslanded == 0 then
		spell:setMsg(282) -- "The [monster] evades." placeholder messageid since blu miss is MIA from the dats
		finaldmg = 0
	elseif params.flat_dmg ~= nil then
		return math.ceil(params.flat_dmg * target:getMod(dsp.mod.PIERCERES) / 1000)
	end

    return finaldmg, taChar;
end;

-- Blue Magical type spells

function BlueMagicalSpell(caster, target, spell, params, statMod)

	local blulvl = 1
	if caster:getMainJob() == dsp.job.BLU then
		blulvl = caster:getMainLvl()
	elseif caster:getSubJob() == dsp.job.BLU then
		blulvl = caster:getSubLvl()
	end

    local D = blulvl + 2;

    if (D > params.duppercap) then
        D = params.duppercap;
    end

    local ST = BlueGetWsc(caster, params); -- According to Wiki ST is the same as WSC, essentially Blue mage spells that are magical use the dmg formula of Magical type Weapon skills

    if (caster:hasStatusEffect(dsp.effect.BURST_AFFINITY)) then
        ST = ST * 2;
    end

    local convergenceBonus = 1.0;
    if (caster:hasStatusEffect(dsp.effect.CONVERGENCE)) then
        convergenceEffect = getStatusEffect(dsp.effect.CONVERGENCE);
        local convLvl = convergenceEffect:getPower();
        if (convLvl == 1) then
            convergenceBonus = 1.05;
        elseif (convLvl == 2) then
            convergenceBonus = 1.1;
        elseif (convLvl == 3) then
            convergenceBonus = 1.15;
        end
    end

    local statBonus = 0;
    local dStat = 0; -- Please make sure to add an additional stat check if there is to be a spell that uses neither INT, MND, or CHR. None currently exist.
    if (statMod == INT_BASED) then -- Stat mod is INT
        dStat = caster:getStat(dsp.mod.INT) - target:getStat(dsp.mod.INT)
        statBonus = (dStat)* params.tMultiplier;
    elseif (statMod == CHR_BASED) then -- Stat mod is CHR
        dStat = caster:getStat(dsp.mod.CHR) - target:getStat(dsp.mod.CHR)
        statBonus = (dStat)* params.tMultiplier;
    elseif (statMod == MND_BASED) then -- Stat mod is MND
        dStat = caster:getStat(dsp.mod.MND) - target:getStat(dsp.mod.MND)
        statBonus = (dStat)* params.tMultiplier;
    end

    D =(((D + ST) * params.multiplier * convergenceBonus) + statBonus)
	
	if params.breath_hp ~= nil and params.breath_lvl ~= nil then
		D = caster:getHP() * params.breath_hp + blulvl * params.breath_lvl
	end

    -- At this point according to wiki we apply standard magic attack calculations

    local magicAttack = 1.0;
    local multTargetReduction = 1.0; -- TODO: Make this dynamically change, temp static till implemented.
    magicAttack = math.floor(D * multTargetReduction);
    
    local rparams = {};
    rparams.diff = dStat;
    rparams.skillType = dsp.skill.BLUE_MAGIC;
    magicAttack = math.floor(magicAttack * applyResistance(caster, target, spell, rparams));
    
    dmg = math.floor(addBonuses(caster, spell, target, magicAttack));
	
	local correlmult = GetMonsterCorrelationMult(params.eco,GetTargetEcosystem(target))
	
	if correlmult > 1 then
		dmg = math.ceil(dmg * correlmult) + math.random(0,4)
	else
		dmg = math.floor(dmg * correlmult)
	end

    caster:delStatusEffectSilent(dsp.effect.BURST_AFFINITY);

    return dmg;
end;

function BlueFinalAdjustments(caster, target, spell, dmg, params, taChar)
    if (dmg < 0) then
        dmg = 0
    end

    dmg = dmg * BLUE_POWER

    dmg = dmg - target:getMod(dsp.mod.PHALANX)
    if (dmg < 0) then
        dmg = 0
    end

    -- handling stoneskin
    dmg = utils.stoneskin(target, dmg)

    target:takeDamage(dmg, caster, dsp.attackType.PHYSICAL, params.dmgtype or dsp.damageType.NONE)
	if not target:isPC() then
		if taChar == nil then
			target:updateEnmityFromDamage(caster,dmg)
		else
			target:updateEnmityFromDamage(taChar,dmg)
		end
	end
    target:handleAfflatusMiseryDamage(dmg)
    -- TP has already been dealt with.
    return dmg;
end;

------------------------------
-- Utility functions below ---
------------------------------

function BlueGetWsc(attacker, params)
    wsc = (attacker:getStat(dsp.mod.STR) * params.str_wsc + attacker:getStat(dsp.mod.DEX) * params.dex_wsc +
         attacker:getStat(dsp.mod.VIT) * params.vit_wsc + attacker:getStat(dsp.mod.AGI) * params.agi_wsc +
         attacker:getStat(dsp.mod.INT) * params.int_wsc + attacker:getStat(dsp.mod.MND) * params.mnd_wsc +
         attacker:getStat(dsp.mod.CHR) * params.chr_wsc) * BlueGetAlpha(attacker:getMainLvl());
    return wsc;
end;

-- Given the raw ratio value (atk/def) and levels, returns the cRatio (min then max)
function BluecRatio(ratio,atk_lvl,def_lvl)
    -- Level penalty...
    local levelcor = 0;
    if (atk_lvl < def_lvl) then
        levelcor = 0.01 * (def_lvl - atk_lvl);
    end
    ratio = ratio - levelcor;

    -- apply caps
    if (ratio<0) then
        ratio = 0;
    elseif (ratio>2) then
        ratio = 2;
    end
	
	--print(string.format("ATK/DEF ratio: %f",ratio))
	
	--[[
    -- Obtaining cRatio_MIN
    local cratiomin = 0;
    if (ratio<1.25) then
        cratiomin = 1.2 * ratio - 0.5;
    elseif (ratio>=1.25 and ratio<=1.5) then
        cratiomin = 1;
    elseif (ratio>1.5 and ratio<=2) then
        cratiomin = 1.2 * ratio - 0.8;
    end
	]]
	local cratiomin = 0.8 + (0.9 * ratio/2) -- 0.8 to 1.7
	
	--print(string.format("cratio min: %f",cratiomin))
	--[[
    -- Obtaining cRatio_MAX
    local cratiomax = 0;
    if (ratio<0.5) then
        cratiomax = 0.4 + 1.2 * ratio;
    elseif (ratio<=0.833 and ratio>=0.5) then
        cratiomax = 1;
    elseif (ratio<=2 and ratio>0.833) then
        cratiomax = 1.2 * ratio;
    end
	]]
	local cratiomax = 1.2 + (0.8 * ratio/2) -- 1.2 to 2.0
	
	--print(string.format("cratio max: %f",cratiomax))
	
    cratio = {};
    if (cratiomin < 0) then
        cratiomin = 0;
    end
    cratio[1] = cratiomin;
    cratio[2] = cratiomax;
    return cratio;
end;

-- Gets the fTP multiplier by applying 2 straight lines between ftp1-ftp2 and ftp2-ftp3
-- tp - The current TP
-- ftp1 - The TP 0% value
-- ftp2 - The TP 150% value
-- ftp3 - The TP 300% value
function BluefTP(tp,ftp1,ftp2,ftp3)
    if (tp >= 0 and tp < 1500) then
        return ftp1 + ( ((ftp2-ftp1)/100) * (tp / 10));
    elseif (tp >= 1500 and tp <= 3000) then
        -- generate a straight line between ftp2 and ftp3 and find point @ tp
        return ftp2 + ( ((ftp3-ftp2)/100) * ((tp-1500) / 10));
    else
        print("blue fTP error: TP value is not between 0-3000!");
    end
    return 1; -- no ftp mod
end;

function BluefSTR(dSTR)
    return math.floor(dSTR/4)-1
	--[[
	local fSTR2 = nil;
    if (dSTR >= 12) then
        fSTR2 = ((dSTR+4)/2);
    elseif (dSTR >= 6) then
        fSTR2 = ((dSTR+6)/2);
    elseif (dSTR >= 1) then
        fSTR2 = ((dSTR+7)/2);
    elseif (dSTR >= -2) then
        fSTR2 = ((dSTR+8)/2);
    elseif (dSTR >= -7) then
        fSTR2 = ((dSTR+9)/2);
    elseif (dSTR >= -15) then
        fSTR2 = ((dSTR+10)/2);
    elseif (dSTR >= -21) then
        fSTR2 = ((dSTR+12)/2);
    else
        fSTR2 = ((dSTR+13)/2);
    end

    return fSTR2;
	]]
end;

function BlueGetHitRate(attacker,target,capHitRate)
    local acc = attacker:getACC();
    local eva = target:getEVA();

    if (attacker:getMainLvl() > target:getMainLvl()) then -- acc bonus!
        acc = acc + ((attacker:getMainLvl()-target:getMainLvl())*3);
    elseif (attacker:getMainLvl() < target:getMainLvl()) then -- acc penalty :(
        acc = acc - ((target:getMainLvl()-attacker:getMainLvl())*1);
    end

    local hitdiff = 0;
    local hitrate = 81;
    if (acc>eva) then
    hitdiff = (acc-eva)/2;
    end
    if (eva>acc) then
    hitdiff = ((-1)*(eva-acc))/3;
    end

    hitrate = hitrate+hitdiff;
    hitrate = hitrate/100;


    -- Applying hitrate caps
    if (capHitRate) then -- this isn't capped for when acc varies with tp, as more penalties are due
        if (hitrate>0.98) then
            hitrate = 0.98;
        end
        if (hitrate<0.4) then
            hitrate = 0.4;
        end
    end
    return hitrate;
end;

-- Function to stagger duration of effects by using the resistance to change the value
function getBlueEffectDuration(caster,resist,effect)
    local duration = 0;

    if (resist == 0.125) then
        resist = 1;
    elseif (resist == 0.25) then
        resist = 2;
    elseif (resist == 0.5) then
        resist = 3;
    else
        resist = 4;
    end

    if (effect == dsp.effect.BIND) then
        duration = math.random(0,5) + resist * 5;
    elseif (effect == dsp.effect.STUN) then
        duration = math.random(2,3) + resist;
        -- printf("Duration of stun is %i",duration);
    elseif (effect == dsp.effect.WEIGHT) then
        duration = math.random(20,24) + resist * 9; -- 20-24
    elseif (effect == dsp.effect.PARALYSIS) then
        duration = math.random(50,60) + resist * 15; -- 50- 60
    elseif (effect == dsp.effect.SLOW) then
        duration = math.random(60,120) + resist * 15; -- 60- 120 -- Needs confirmation but capped max duration based on White Magic Spell Slow
    elseif (effect == dsp.effect.SILENCE) then
        duration = math.random(60,180) + resist * 15; -- 60- 180 -- Needs confirmation but capped max duration based on White Magic Spell Silence
    elseif (effect == dsp.effect.POISON) then
        duration = math.random(20,30) + resist * 9; -- 20-30 -- based on magic spell poison
    end

    return duration;
end;

-- obtains alpha, used for working out WSC
function BlueGetAlpha(level)
    local alpha = 1.00;
    if (level <= 5) then
        alpha = 1.00;
    elseif (level <= 11) then
        alpha = 0.99;
    elseif (level <= 17) then
        alpha = 0.98;
    elseif (level <= 23) then
        alpha = 0.97;
    elseif (level <= 29) then
        alpha = 0.96;
    elseif (level <= 35) then
        alpha = 0.95;
    elseif (level <= 41) then
        alpha = 0.94;
    elseif (level <= 47) then
        alpha = 0.93;
    elseif (level <= 53) then
        alpha = 0.92;
    elseif (level <= 59) then
        alpha = 0.91;
    elseif (level <= 61) then
        alpha = 0.90;
    elseif (level <= 63) then
        alpha = 0.89;
    elseif (level <= 65) then
        alpha = 0.88;
    elseif (level <= 67) then
        alpha = 0.87;
    elseif (level <= 69) then
        alpha = 0.86;
    elseif (level <= 71) then
        alpha = 0.85;
    elseif (level <= 73) then
        alpha = 0.84;
    elseif (level <= 75) then
        alpha = 0.83;
    elseif (level <= 99) then
        alpha = 0.85;
    end
    return alpha;
end;

