-----------------------------------------
-- Spell: Barsleepra
-----------------------------------------
require("scripts/globals/spells/barstatus")
require("scripts/globals/status")
-----------------------------------------

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
	local dseal = 0
	if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
		dseal = 1
	end
    return applyBarstatus(dsp.effect.BARSLEEP,caster,target,spell,dseal)
end
