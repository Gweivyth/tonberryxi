-----------------------------------------
-- Spell: Reraise 3
-----------------------------------------

require("scripts/globals/status")

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
    local rlvl = 3
	
	if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
        rlvl = 13
    end
	
    target:delStatusEffect(dsp.effect.RERAISE)
    target:addStatusEffect(dsp.effect.RERAISE,rlvl,0,3600) --reraise 3, 30min duration

    return dsp.effect.RERAISE
end
