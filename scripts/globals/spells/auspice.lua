-----------------------------------------
--
-- Spell: Auspice
--
-----------------------------------------
require("scripts/globals/magic")
require("scripts/globals/status")
-----------------------------------------

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)
    local effect = dsp.effect.AUSPICE
	local dseal = false
	if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
		dseal = true
	end
    doEnspell(caster, target, spell, effect, dseal)
    return effect
end