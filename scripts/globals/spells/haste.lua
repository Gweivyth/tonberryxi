-----------------------------------------
-- Spell: Haste
-- Composure increases duration 3x
-----------------------------------------
require("scripts/globals/magic")
require("scripts/globals/msg")
require("scripts/globals/status")
-----------------------------------------

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)
    local duration = calculateDuration(180, spell:getSkillType(), spell:getSpellGroup(), caster, target)
    duration = calculateDurationForLvl(duration, 48, target:getMainLvl())

    local power = 1465 -- 150/1024 ~14.65%
	if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL) and caster:getID() ~= target:getID()) then
		if caster:getID() == target:getID() then
			power = 2285
		else
			power = 2955
		end
	end

    if not target:addStatusEffect(dsp.effect.HASTE, power, 0, duration) then
        spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT)
    end

    return dsp.effect.HASTE
end
