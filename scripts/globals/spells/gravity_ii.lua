-----------------------------------------
-- Spell: Gravity II
-----------------------------------------
require("scripts/globals/magic")
require("scripts/globals/msg")
require("scripts/globals/status")
-----------------------------------------

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)
    -- Pull base stats.
    local dINT = caster:getStat(dsp.mod.INT) - target:getStat(dsp.mod.INT)

    local power = calculatePotency(32, spell:getSkillType(), caster, target)
	
	if (caster:isPC()) then
		power = power * 3
	end

    -- Duration, including resistance.  Unconfirmed.
    local duration = calculateDuration(180, spell:getSkillType(), spell:getSpellGroup(), caster, target)

    local params = {}
    params.diff = dINT
    params.skillType = dsp.skill.ENFEEBLING_MAGIC
    params.bonus = 0
    params.effect = dsp.effect.WEIGHT
    local resist = applyResistanceEffect(caster, target, spell, params)

    if resist >= 0.5 then --Do it!
        if target:addStatusEffect(params.effect, power, 0, duration * resist) then
            spell:setMsg(dsp.msg.basic.MAGIC_ENFEEB_IS)
			local downamount = 15
			local eva = target:getEVA()
			if (eva < 15) then
				downamount = eva
			end
			target:addStatusEffect(dsp.effect.EVASION_DOWN, downamount, 0, duration * resist)
        else
            spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT)
        end
    else
        spell:setMsg(dsp.msg.basic.MAGIC_RESIST_2)
    end

    return params.effect
end