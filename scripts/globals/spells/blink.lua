-----------------------------------------
-- Spell: Blink
-----------------------------------------
require("scripts/globals/magic")
require("scripts/globals/msg")
require("scripts/globals/status")
-----------------------------------------

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)
    local duration = calculateDuration(300, spell:getSkillType(), spell:getSpellGroup(), caster, target)
	
	local numshadows = 2
	if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
		numshadows = 7
	end
	
    if target:addStatusEffect(dsp.effect.BLINK, numshadows, 0, duration) then
        spell:setMsg(dsp.msg.basic.MAGIC_GAIN_EFFECT)
    else
        spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT)
    end

    return dsp.effect.BLINK
end
