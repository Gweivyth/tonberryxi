-----------------------------------------
-- Spell: Stonaga
-- Deals earth damage to enemies within area of effect.
-----------------------------------------
require("scripts/globals/status")
require("scripts/globals/magic")
-----------------------------------------

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)
    local spellParams = {}
    spellParams.hasMultipleTargetReduction = true
    spellParams.resistBonus = 1.0
	spellParams.MPcost = 24
	spellParams.efficiency = 1.6 * 0.45 * 1.7
	spellParams.variance = 1.1
	spellParams.tier = 1
    spellParams.V = 56
    spellParams.V0 = 60
    spellParams.V50 = 210
    spellParams.V100 = 310
    spellParams.V200 = 410
    spellParams.M = 1
    spellParams.M0 = 3
    spellParams.M50 = 2
    spellParams.M100 = 1
    spellParams.M200 = 0
    spellParams.I = 74

    return doElementalNuke(caster, spell, target, spellParams)
end
