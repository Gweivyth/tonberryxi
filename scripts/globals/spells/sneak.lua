-----------------------------------------
-- Spell: Sneak
-- Lessens chance of being detected by sound
-- Duration is random number between 30 seconds and 5 minutes
-----------------------------------------
require("scripts/globals/magic")
require("scripts/globals/msg")
require("scripts/globals/settings")
require("scripts/globals/status")

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
    if (not target:hasStatusEffect(dsp.effect.SNEAK)) then

        local duration = math.random(80, 160)
		duration = calculateDurationForLvl(duration, 20, target:getMainLvl())
		
        -- FIXME: Create mod and use that instead of an itemID check
        if (target:getEquipID(dsp.slot.BACK) == 13692) then -- skulker's cape
            duration = duration * 1.5
        end
		
		if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
			duration = duration * 2 * 1.25
			if not target:hasStatusEffect(dsp.effect.DEODORIZE) then
				target:addStatusEffect(dsp.effect.DEODORIZE, 0, 10, math.floor(duration * SNEAK_INVIS_DURATION_MULTIPLIER))
			end
			if not target:hasStatusEffect(dsp.effect.INVISIBLE) then
				target:addStatusEffect(dsp.effect.INVISIBLE, 0, 10, math.floor(duration * SNEAK_INVIS_DURATION_MULTIPLIER))
			end
		end
        
        spell:setMsg(dsp.msg.basic.MAGIC_GAIN_EFFECT)
        target:addStatusEffect(dsp.effect.SNEAK, 0, 10, math.floor(duration * SNEAK_INVIS_DURATION_MULTIPLIER))
    else
        spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT) -- no dsp.effect.
    end

    return dsp.effect.SNEAK
end
