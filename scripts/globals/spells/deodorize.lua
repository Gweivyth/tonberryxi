-----------------------------------------
-- Spell: Deodorize
-- Lessens chance of being detected by smell.
-- Duration is random number between 30 seconds and 5 minutes
-----------------------------------------
require("scripts/globals/magic")
require("scripts/globals/msg")
require("scripts/globals/settings")
require("scripts/globals/status")

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)
    if not target:hasStatusEffect(dsp.effect.DEODORIZE) then
	
        local duration = math.random(160, 320)
        duration = calculateDurationForLvl(duration, 15, target:getMainLvl())
		
		
		
		
		
		
		if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
		
			if not target:hasStatusEffect(dsp.effect.INVISIBLE) then
				target:addStatusEffect(dsp.effect.INVISIBLE, 0, 10, math.floor(duration * SNEAK_INVIS_DURATION_MULTIPLIER))
			end
			if not target:hasStatusEffect(dsp.effect.SNEAK) then
				target:addStatusEffect(dsp.effect.SNEAK, 0, 10, math.floor(duration * SNEAK_INVIS_DURATION_MULTIPLIER))
			end
		end

        spell:setMsg(dsp.msg.basic.MAGIC_GAIN_EFFECT)
        target:addStatusEffect(dsp.effect.DEODORIZE, 0, 10, math.floor(duration * SNEAK_INVIS_DURATION_MULTIPLIER))
    else
        spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT)
    end

    return dsp.effect.DEODORIZE
end
