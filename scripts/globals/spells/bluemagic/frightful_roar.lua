-----------------------------------------
-- Spell: Frightful Roar
-- Weakens defense of enemies within range
-- Spell cost: 32 MP
-- Monster Type: Demon
-- Spell Type: Magical (Wind)
-- Blue Magic Points: 3
-- Stat Bonus: AGI+2
-- Level: 50
-- Casting Time: 2 seconds
-- Recast Time: 20 seconds
-- Magic Bursts on: Detonation, Fragmentation, and Light
-- Combos: Auto Refresh
-----------------------------------------
require("scripts/globals/bluemagic")
require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/msg")
-----------------------------------------

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
    local params = {}
    params.attribute = dsp.mod.INT
    params.skillType = dsp.skill.BLUE_MAGIC
    params.effect = dsp.effect.DEFENSE_DOWN
	params.eco = ECO_DEMON
    local resist = applyResistance(caster, target, spell, params)
    local duration = 60 * resist
    local power = 10
	
	local cor = GetMonsterCorrelationMult(params.eco,GetTargetEcosystem(target))
	local roll = 0.95
	
	if cor > 1 then
		roll = 1.0
	elseif cor < 1 then
		roll = 0.65
	end

    if (resist > 0.5 and math.random() < roll) then -- Do it!
        if (target:addStatusEffect(params.effect,power,0,duration)) then
            spell:setMsg(dsp.msg.basic.MAGIC_ENFEEB_IS)
        else
            spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT)
        end
    else
        spell:setMsg(dsp.msg.basic.MAGIC_RESIST)
    end

    return params.effect
end
