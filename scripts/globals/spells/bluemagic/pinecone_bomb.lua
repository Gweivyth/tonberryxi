-----------------------------------------
-- Spell: Pinecomb Bomb
-- Additional effect: Sleep. Duration of effect varies with TP
-- Spell Type: Physical (Piercing)
-- Level: 36
-- Skillchain Element(s): Liquefaction
-- Combos: None
-----------------------------------------
require("scripts/globals/bluemagic")
require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/msg")
-----------------------------------------

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
    local params = {}
	local eparams = {}
    -- This data should match information on http://wiki.ffxiclopedia.org/wiki/Calculating_Blue_Magic_Damage
        params.tpmod = TPMOD_CRITICAL
        params.dmgtype = DMGTYPE_PIERCE
        params.scattr = SC_LIQUEFACTION
        params.numhits = 1
        params.multiplier = 1.25
        params.tp150 = 1.25
        params.tp300 = 1.25
        params.azuretp = 1.35
        params.duppercap = 22
        params.str_wsc = 0.0
        params.dex_wsc = 0.0
        params.vit_wsc = 0.0
        params.agi_wsc = 0.0
        params.int_wsc = 0.20
        params.mnd_wsc = 0.0
        params.chr_wsc = 0.0
		params.SA = 1.4
		params.eco = ECO_PLANTOID
	eparams.diff = nil
    eparams.attribute = dsp.mod.INT
    eparams.skillType = dsp.skill.BLUE_MAGIC
    eparams.bonus = 0
    eparams.effect = dsp.effect.SLEEP_I
	local resist = applyResistanceEffect(caster, target, spell, eparams)
    local duration = 60 * resist
	
    local damage, taChar = BluePhysicalSpell(caster, target, spell, params)
	if damage == 0 then
		return 0
	end
    damage = BlueFinalAdjustments(caster, target, spell, damage, params, taChar)
	
	local cor = GetMonsterCorrelationMult(params.eco,GetTargetEcosystem(target))
	local roll = 0.55
	
	if cor > 1 then
		roll = 0.78
	elseif cor < 1 then
		roll = 0.35
	end

    if damage > 0 then
		target:delStatusEffectsByFlag(dsp.effectFlag.DAMAGE) -- since it's disabled in cpp for this spell
		if target:hasStatusEffect(dsp.effect.BIND) then
			target:delStatusEffect(dsp.effect.BIND)
		end
		if resist > 0.5 and math.random() < roll then
			target:addStatusEffect(dsp.effect.SLEEP_I,3,0,duration)
		end
    end

    return damage
end
