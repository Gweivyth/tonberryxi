-----------------------------------------
-- Spell: Radiant Breath
-- Deals light damage to enemies within a fan-shaped area of effect originating from the caster. Additional effect: Slow and Silence.
-- Spell cost: 116 MP
-- Monster Type: Wyverns
-- Spell Type: Magical (Light)
-- Blue Magic Points: 4
-- Stat Bonus: CHR+1, HP+5
-- Level: 54
-- Casting Time: 5.25 seconds
-- Recast Time: 33.75 seconds
-- Magic Bursts on: Transfixion, Fusion, Light
-- Combos: None
-----------------------------------------
require("scripts/globals/bluemagic")
require("scripts/globals/status")
require("scripts/globals/magic")
-----------------------------------------

function onMagicCastingCheck(caster,target,spell)
	if not caster:isFacing(target) then
		return 5
	end
    return 0
end

function onSpellCast(caster,target,spell)
    local multi = 2.90
    if (caster:hasStatusEffect(dsp.effect.AZURE_LORE)) then
        multi = multi + 0.50
    end

    local params = {}
    -- This data should match information on http://wiki.ffxiclopedia.org/wiki/Calculating_Blue_Magic_Damage
        params.multiplier = multi
        params.tMultiplier = 1.5
        params.duppercap = 69
        params.str_wsc = 0.0
        params.dex_wsc = 0.0
        params.vit_wsc = 0.0
        params.agi_wsc = 0.0
        params.int_wsc = 0.0
        params.mnd_wsc = 0.0
        params.chr_wsc = 0.0
        params.diff = caster:getStat(dsp.mod.INT) - target:getStat(dsp.mod.INT)
        params.attribute = dsp.mod.INT
        params.skillType = dsp.skill.BLUE_MAGIC
        params.bonus = 1.0
		params.breath_hp = 0.2
		params.breath_lvl = 1.33
		params.eco = ECO_DRAGON
    local resist = applyResistance(caster, target, spell, params)
    local damage = BlueMagicalSpell(caster, target, spell, params, nil)
    damage = BlueFinalAdjustments(caster, target, spell, damage, params)
	
	local cor = GetMonsterCorrelationMult(params.eco,GetTargetEcosystem(target))
	local roll = 0.90
	
	if cor > 1 then
		roll = 0.98
	elseif cor < 1 then
		roll = 0.65
	end

    if (damage > 0 and resist > 0.3 and math.random() < roll) then
    local typeEffect = dsp.effect.SLOW
        target:delStatusEffect(typeEffect)
        target:addStatusEffect(typeEffect,3500,0,getBlueEffectDuration(caster,resist,typeEffect))
    end

    if (damage > 0 and resist > 0.3 and math.random() < roll) then
        target:delStatusEffect(dsp.effect.SILENCE)
        target:addStatusEffect(dsp.effect.SILENCE,25,0,getBlueEffectDuration(caster,resist,dsp.effect.SILENCE))
    end

    return damage
end
