-----------------------------------------
-- Spell: Voracious Trunk
-- Steals an enemy's buff
-- Spell cost: 72 MP
-- Monster Type: Beasts
-- Spell Type: Magical (Wind)
-- Blue Magic Points: 4
-- Stat Bonus: AGI +2
-- Level: 72
-- Casting Time: 10 seconds
-- Recast Time: 56 seconds
-- Combos: Auto Refresh
-----------------------------------------
require("scripts/globals/bluemagic")
require("scripts/globals/magic")
require("scripts/globals/status")
require("scripts/globals/msg")
-----------------------------------------

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)
    local resist = applyResistanceAbility(caster, target, dsp.magic.ele.WIND, 0, 0)
	local params = {}
	params.eco = ECO_BEAST
    local stolen = 0
	
	local cor = GetMonsterCorrelationMult(params.eco,GetTargetEcosystem(target))
	local roll = 0.87
	
	if cor > 1 then
		roll = 0.97
	elseif cor < 1 then
		roll = 0.53
	end

    if resist > 0.0625 and math.random() < roll then
        stolen = caster:stealStatusEffect(target)
        if stolen ~= 0 then
            spell:setMsg(dsp.msg.basic.MAGIC_STEAL)
        else
            spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT)
        end
    else
        spell:setMsg(dsp.msg.basic.MAGIC_RESIST)
    end
    
    return stolen
end
