-----------------------------------------
-- Spell: Repose
-----------------------------------------
require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/msg")
-----------------------------------------

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
    local dMND = (caster:getStat(dsp.mod.MND) - target:getStat(dsp.mod.MND))
    local params = {}
    params.diff = nil
    params.attribute = dsp.mod.MND
    params.skillType = dsp.skill.DIVINE_MAGIC
    params.bonus = 0
    params.effect = dsp.effect.SLEEP_II

	if target:isUndead() then -- +40MACC vs undead, +15MACC vs undeadNM, -200MACC vs any other mob
		if not target:isNM() then
			params.bonus = 40
		else
			params.bonus = 15
		end
	else
		params.bonus = -200
	end
	
	if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
		params.bonus = params.bonus + 40
	end

    local resist = applyResistanceEffect(caster, target, spell, params)
	
	if not target:isUndead() and math.random() < 0.45 then
		resist = 0
	end
	
    if (resist < 0.5) then
        spell:setMsg(dsp.msg.basic.MAGIC_RESIST) -- Resist
        return dsp.effect.SLEEP_II
    end

    if (target:addStatusEffect(dsp.effect.SLEEP_II,2,0,90*resist)) then
        spell:setMsg(dsp.msg.basic.MAGIC_ENFEEB)
    else
        spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT) -- No effect
    end

    return dsp.effect.SLEEP_II
end
