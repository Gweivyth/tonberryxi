-----------------------------------------
-- Spell: Holy
-- Deals light damage to an enemy.
-----------------------------------------
require("scripts/globals/status")
require("scripts/globals/magic")
-----------------------------------------

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
    -- doDivineNuke(V,M,caster,spell,target,hasMultipleTargetReduction,resistBonus)
    local params = {}
    params.dmg = 500
    params.multiplier = 1
    params.hasMultipleTargetReduction = false
    params.resistBonus = 0
	
	if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
		params.dmg = 226
	end
	print(string.format("%i",caster:getMod(dsp.mod.AFFLATUS_SOLACE)))
	--if caster:hasStatusEffect(dsp.effect.AFFLATUS_SOLACE) then
		local mult = 1 + caster:getMod(dsp.mod.AFFLATUS_SOLACE)/375
		params.dmg = math.floor(params.dmg * mult)
		caster:setMod(dsp.mod.AFFLATUS_SOLACE,0)
	--end
	
    dmg = doDivineBanishNuke(caster, target, spell, params)
	
    return dmg
end