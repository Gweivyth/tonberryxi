-----------------------------------------
-- Spell: Poisona
-- Removes poison from target.
-----------------------------------------
require("scripts/globals/status")
require("scripts/globals/spells/barstatus")
require("scripts/globals/msg")
-----------------------------------------

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
    if (target:delStatusEffect(dsp.effect.POISON)) then
		if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
			applyBarstatus(dsp.effect.BARPOISON,caster,target,spell,2)
		end
		spell:setMsg(dsp.msg.basic.MAGIC_REMOVE_EFFECT)
    else
        spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT)
    end
    return dsp.effect.POISON
end
