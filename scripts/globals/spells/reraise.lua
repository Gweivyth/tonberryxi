-----------------------------------------
-- Spell: Reraise
-----------------------------------------

require("scripts/globals/status")

function onMagicCastingCheck(caster,target,spell)
    return 0
end

function onSpellCast(caster,target,spell)
    local rlvl = 1
	
	if (caster:hasStatusEffect(dsp.effect.DIVINE_SEAL)) then
        rlvl = 11
    end
	
    target:addStatusEffect(dsp.effect.RERAISE,rlvl,0,3600) --reraise 1, 30min duration

    return dsp.effect.RERAISE
end
