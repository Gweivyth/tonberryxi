-----------------------------------------
-- Spell: Refresh
-- Gradually restores target party member's MP
-- Composure increases duration 3x
-----------------------------------------
require("scripts/globals/magic")
require("scripts/globals/msg")
require("scripts/globals/status")
-----------------------------------------

function onMagicCastingCheck(caster, target, spell)
    return 0
end

function onSpellCast(caster, target, spell)

	local enhancingmod = caster:getSkillLevel(34) - 121; -- 34 is enhancing magic skill
	
	-- for RDM, enhancing caps @ 121 lv 41 and 256 lv 75, difference is 135
	-- for RDM, enhancing caps @ 121 lv 41 and 196 lv 60, difference is 75
	
	--if (enhancingmod < 0) then
		--enhancingmod = enhancingmod * 2; -- penalty is twice as severe
	--end
	
	enhancingmod = math.ceil((enhancingmod / 75) * 30); -- gaining 30 seconds on refresh at 60, but 0 seconds at 41, assuming capped enhancing
	
	local duration = 120 + enhancingmod;
	if (duration < 110) then
		duration = 110
	end
	if (duration > 150) then
		duration = 150
	end
    duration = calculateDuration(duration, spell:getSkillType(), spell:getSpellGroup(), caster, target)
	duration = calculateDurationForLvl(duration, 41, target:getMainLvl())

    local mp = 3 + caster:getMod(dsp.mod.ENHANCES_REFRESH)

    if target:hasStatusEffect(dsp.effect.SUBLIMATION_ACTIVATED) then
        spell:setMsg(dsp.msg.basic.MAGIC_NO_EFFECT)
        return 0
    end

    target:delStatusEffect(dsp.effect.REFRESH)
    target:addStatusEffect(dsp.effect.REFRESH, mp, 0, duration)

    return dsp.effect.REFRESH
end
