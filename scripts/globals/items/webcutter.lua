-----------------------------------------
-- ID: 18040
-- Item: Webcutter
-- Additional Effect: Stuns Vermin
-- THANKS FARTHEKILLER!
----------------------------------
require("scripts/globals/status")
require("scripts/globals/magic")
require("scripts/globals/msg")
-----------------------------------

function onAdditionalEffect(player,target,damage)
    if target:getSystem() == dsp.ecosystem.VERMIN then
        local chance = 15
        if math.random(100) <= chance and applyResistanceAddEffect(player,target,dsp.magic.ele.LIGHTNING,0) > 0.5 then
            target:addStatusEffect(dsp.effect.STUN, 2, 0, 3)
            return dsp.subEffect.STUN, dsp.msg.basic.ADD_EFFECT_STATUS, dsp.effect.STUN
        end
    end
    return 0, 0, 0
end