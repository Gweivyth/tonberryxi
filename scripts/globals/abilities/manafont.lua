-----------------------------------
-- Ability: Manafont
-- Eliminates the cost of magic spells.
-- Obtained: Black Mage Level 1
-- Recast Time: 1:00:00
-- Duration: 0:01:00
-----------------------------------
require("scripts/globals/settings")
require("scripts/globals/status")
-----------------------------------

function onAbilityCheck(player,target,ability)
    return 0,0
end

function onUseAbility(player, target, ability)
	local duration = 60;

	if (player:hasStatusEffect(dsp.effect.CHAINSPELL)) then
		duration = 15;
	end

    player:addStatusEffect(dsp.effect.MANAFONT,1,0,duration)
end