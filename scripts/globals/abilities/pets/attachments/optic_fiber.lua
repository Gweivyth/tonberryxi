-----------------------------------
-- Attachment: Optic Fiber
-- Increases the performance of other attachments by a percentage
-----------------------------------
require("scripts/globals/automaton")
require("scripts/globals/status")

function onEquip(pet)
    --pet:addMod(dsp.mod.AUTO_PERFORMANCE_BOOST, 10)
	updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 10)
end

function onUnequip(pet)
    --pet:delMod(dsp.mod.AUTO_PERFORMANCE_BOOST, 10)
	updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 0)
end

function onManeuverGain(pet,maneuvers)
    if maneuvers == 1 then
        --pet:addMod(dsp.mod.AUTO_PERFORMANCE_BOOST, 10)
		updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 20)
    elseif maneuvers == 2 then
        --:addMod(dsp.mod.AUTO_PERFORMANCE_BOOST, 5)
		updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 25)
    elseif maneuvers == 3 then
        --pet:addMod(dsp.mod.AUTO_PERFORMANCE_BOOST, 5)
		updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 30)
    end
    local master = pet:getMaster()
    if master then
		master:updateAttachments()
    end
end

function onManeuverLose(pet,maneuvers)
	
    if maneuvers == 1 then
        --pet:delMod(dsp.mod.AUTO_PERFORMANCE_BOOST, 10)
		updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 20)
    elseif maneuvers == 2 then
        --pet:delMod(dsp.mod.AUTO_PERFORMANCE_BOOST, 5)
		updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 25)
    elseif maneuvers == 3 then
        --pet:delMod(dsp.mod.AUTO_PERFORMANCE_BOOST, 5)
		updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 30)
	elseif maneuvers == 0 then
		updateModPerformance(pet, dsp.mod.AUTO_PERFORMANCE_BOOST, 'fiberp', 10)
    end
    local master = pet:getMaster()
    if master then
        master:updateAttachments()
    end
	
end
