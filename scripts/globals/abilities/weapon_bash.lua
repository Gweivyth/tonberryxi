-----------------------------------
-- Ability: Weapon Bash
-- Delivers an attack that can stun the target.
-- Obtained: Dark Knight Level 20
-- Cast Time: Instant
-- Recast Time: 3:00 minutes
-- Note: This ability requires a two-handed weapon to use
-----------------------------------
require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/msg")
-----------------------------------

function onAbilityCheck(player,target,ability)
    if (not player:isWeaponTwoHanded()) then
        return dsp.msg.basic.NEEDS_2H_WEAPON,0
    else
        return 0,0
    end
end

function onUseAbility(player,target,ability)
    -- Applying Weapon Bash stun. Rate is said to be near 100%, so let's say 99%.
	
	local duration = 6;
	
	local chance = 78 + (player:getMainLvl() - target:getMainLvl())*5
	
	if (chance < 10) then
		chance = 10
	end
	
	if (math.random(0,100) > chance) then
		duration = math.random(1,4)
	end
	
    if (math.random(0,100) < (98 + player:getMainLvl() - target:getMainLvl())) then
		player:addTP(40)
        target:addStatusEffect(dsp.effect.STUN,1,0,duration)
    end

    -- Weapon Bash deals damage dependant of Dark Knight level
    local darkKnightLvl = 0
    if (player:getMainJob() == dsp.job.DRK) then
        darkKnightLvl = player:getMainLvl()    -- Use Mainjob Lvl
    elseif (player:getSubJob() == dsp.job.DRK) then
        darkKnightLvl = player:getSubLvl()    -- Use Subjob Lvl
    end

    -- Calculating and applying Weapon Bash damage
    local damage = math.floor(((darkKnightLvl + 11) / 4) + player:getMod(dsp.mod.WEAPON_BASH))
    target:takeDamage(damage, player, dsp.attackType.PHYSICAL, dsp.damageType.BLUNT)
    target:updateEnmityFromDamage(player,damage)
	
	ability:setMsg(dsp.msg.basic.JA_DAMAGE)
	
    return damage
end
