-----------------------------------
-- Ability: Hide
-- User becomes invisible.
-- Obtained: Thief Level 45
-- Recast Time: 5:00
-- Duration: Random
-----------------------------------
require("scripts/globals/settings")
require("scripts/globals/status")
-----------------------------------

function onAbilityCheck(player,target,ability)
    return 0,0
end

function onUseAbility(player,target,ability)
    local duration = math.random(60, 180)
    duration = duration * (1 + player:getMod(dsp.mod.HIDE_DURATION)/100)
    player:addStatusEffect(dsp.effect.HIDE, 1, 0, math.floor(duration * SNEAK_INVIS_DURATION_MULTIPLIER))
	
	local cursorta = player:getCursorTarget()
	
	if cursorta ~= nil and cursorta:isPC() and cursorta:isEngaged() then
		print("ooooo")
		local tartar = cursorta:getTarget()
		if tartar:getID() == player:getID() then
			print("aaaa")
			cursorta:disengage()
		end
	end
	
end