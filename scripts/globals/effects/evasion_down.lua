-----------------------------------
--
-- dsp.effect.EVASION_DOWN
--
-----------------------------------
require("scripts/globals/status")
-----------------------------------

function onEffectGain(target,effect)
    local eva = target:getMod(dsp.mod.EVA)
	if target:isPC() then
		eva = target:getEVA()
	end
	
	if (eva - effect:getPower() < 0) then
        effect:setPower(eva)
    end
    target:addMod(dsp.mod.EVA,-effect:getPower())
end

function onEffectTick(target,effect)
    -- Only Feint uses the tick, restore 10 evasion every tick
    local evaDownAmt = effect:getPower()
    if (evaDownAmt > 0) then
        effect:setPower(evaDownAmt - 10)
        target:delMod(dsp.mod.EVA, -10)
    end
end

function onEffectLose(target,effect)
    local evaDownAmt = effect:getPower()
    if (evaDownAmt > 0) then
        target:delMod(dsp.mod.EVA,-effect:getPower())
    end
end
