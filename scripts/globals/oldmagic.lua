function doElementalNuke(caster, spell, target, spellParams)
    local DMG = 0;
    local dINT = caster:getStat(dsp.mod.INT) - target:getStat(dsp.mod.INT);
    local V = 0;
    local M = 0;

    if (USE_OLD_MAGIC_DAMAGE and spellParams.V ~= nil and spellParams.M ~= nil) then
        V = 2.5 * spellParams.MPcost * spellParams.efficiency; -- base damage, 2.5 affixes MP cost directly to mp efficiency
        M = 2;
		
		if (dINT < 0) then
			M = 2 - ((spellParams.tier - 1)*0.2); -- penalty is 2,1.8,1.6,1.4 at tiers 1,2,3,4
		end
		
		variance = (math.random()*(spellParams.variance-1))+1
		
		if (math.random(1,2) == 1) then
			variance = 1/variance;
		end
		
        local cap = spellParams.tier * 10;
		
		if (spellParams.tier == 5) then
			cap = 100; -- for ancient magic
		end
		
		if (dINT > cap) then
			dINT = cap;
		end
		
		DMG = V + (dINT * M) * variance;

        if DMG < 0 then
			DMG = 0;
		end
    else
        local hasMultipleTargetReduction = spellParams.hasMultipleTargetReduction; --still unused!!!
        local resistBonus = spellParams.resistBonus;
        local AMIIaccBonus = spellParams.AMIIaccBonus;
        local mDMG = caster:getMod(dsp.mod.MAGIC_DAMAGE);

        --[[
                Calculate base damage:
                D = mDMG + V + (dINT × M)
                D is then floored
                For dINT reduce by amount factored into the V value (example: at 134 INT, when using V100 in the calculation, use dINT = 134-100 = 34)
        ]]

        if (dINT <= 49) then
            V = spellParams.V0; -- stone: 10
            M = spellParams.M0; -- stone: 2
            DMG = math.floor(DMG + mDMG + V + (dINT * M));

            if (DMG <= 0) then
                return 0;
            end

        elseif (dINT >= 50 and dINT <= 99) then
            V = spellParams.V50;
            M = spellParams.M50;
            DMG = math.floor(DMG + mDMG + V + ((dINT - 50) * M));

        elseif (dINT >= 100 and dINT <= 199) then
            V = spellParams.V100;
            M = spellParams.M100;
            DMG = math.floor(DMG + mDMG + V + ((dINT - 100) * M));

        elseif (dINT > 199) then
            V = spellParams.V200;
            M = spellParams.M200;
            DMG = math.floor(DMG + mDMG + V + ((dINT - 200) * M));
        end
    end

    --get resist multiplier (1x if no resist)
    local params = {};
    params.attribute = dsp.mod.INT;
    params.skillType = dsp.skill.ELEMENTAL_MAGIC;
    params.resistBonus = resistBonus;
    params.AMIIaccBonus = AMIIaccBonus;

    local resist = applyResistance(caster, target, spell, params);

    --get the resisted damage
    DMG = DMG * resist;

    --add on bonuses (staff/day/weather/jas/mab/etc all go in this function)
    DMG = addBonuses(caster, spell, target, DMG, spellParams);

    --add in target adjustment
    local ele = spell:getElement();
    DMG = adjustForTarget(target, DMG, ele);

    --add in final adjustments
    DMG = finalMagicAdjustments(caster, target, spell, DMG);

    return DMG;
end