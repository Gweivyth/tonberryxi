require("scripts/globals/settings")
require("scripts/globals/status")
require("scripts/globals/zone")
-----------------------------------

-----------------------------------
-- public functions
-----------------------------------

-- called by core after a player logs into the server or zones
function friendListMain(player,arg)
    if not zoning then
        -- things checked ONLY during logon go here
        if firstLogin then
            CharCreate(player)
        end
    else
        -- things checked ONLY during zone in go here
    end

    -- apply mods from gearsets (scripts/globals/gear_sets.lua)
    checkForGearSet(player)

    -- god mode
    if player:getVar("GodMode") == 1 then
        player:addStatusEffect(dsp.effect.MAX_HP_BOOST,1000,0,0)
        player:addStatusEffect(dsp.effect.MAX_MP_BOOST,1000,0,0)
        player:addStatusEffect(dsp.effect.MIGHTY_STRIKES,1,0,0)
        player:addStatusEffect(dsp.effect.HUNDRED_FISTS,1,0,0)
        player:addStatusEffect(dsp.effect.CHAINSPELL,1,0,0)
        player:addStatusEffect(dsp.effect.PERFECT_DODGE,1,0,0)
        player:addStatusEffect(dsp.effect.INVINCIBLE,1,0,0)
        player:addStatusEffect(dsp.effect.ELEMENTAL_SFORZO,1,0,0)
        player:addStatusEffect(dsp.effect.MANAFONT,1,0,0)
        player:addStatusEffect(dsp.effect.REGAIN,300,0,0)
        player:addStatusEffect(dsp.effect.REFRESH,99,0,0)
        player:addStatusEffect(dsp.effect.REGEN,99,0,0)
        player:addMod(dsp.mod.RACC,2500)
        player:addMod(dsp.mod.RATT,2500)
        player:addMod(dsp.mod.ACC,2500)
        player:addMod(dsp.mod.ATT,2500)
        player:addMod(dsp.mod.MATT,2500)
        player:addMod(dsp.mod.MACC,2500)
        player:addMod(dsp.mod.RDEF,2500)
        player:addMod(dsp.mod.DEF,2500)
        player:addMod(dsp.mod.MDEF,2500)
        player:addHP(50000)
        player:setMP(50000)
    end

    -- !hide
    if player:getVar("GMHidden") == 1 then
        player:setGMHidden(true)
    end

    -- remember time player zoned in (e.g., to support zone-in delays)
    player:setLocalVar("ZoneInTime", os.time())
end

function onPlayerLevelUp(player)
end

function onPlayerLevelDown(player)
end
