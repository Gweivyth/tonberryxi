---------------------------------------------------------------------------------------------------
-- func: addkeyitem <ID> <player>
-- desc: Sends a server msg to everyone online
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 3,
    parameters = "ssssssssssssssssssssssssssssssss"
};

function error(player, msg)
    --player:PrintToPlayer(msg);
    --player:PrintToPlayer("!addkeyitem <key item ID> {player}");
end;

function onTrigger(player, string1, string2, string3, string4, string5, string6, string7, string8, string9, string10, string11, string12, string13, string14, string15, string16, string17, string18, string19, string20, string21, string22, string23, string24, string25, string26, string27, string28, string29, string30, string31, string32)
	
	if (string1 == nil) then
		player:PrintToPlayer("error -- message is blank")
		return 1
	end
	
	if (string2 ~= nil) then
		string1 = string1 .. " " .. string2
	end
	
	if (string3 ~= nil) then
		string1 = string1 .. " " .. string3
	end
	
	if (string4 ~= nil) then
		string1 = string1 .. " " .. string4
	end
	
	if (string5 ~= nil) then
		string1 = string1 .. " " .. string5
	end
	
	if (string6 ~= nil) then
		string1 = string1 .. " " .. string6
	end
	
	if (string7 ~= nil) then
		string1 = string1 .. " " .. string7
	end
	
	if (string8 ~= nil) then
		string1 = string1 .. " " .. string8
	end
	
	if (string9 ~= nil) then
		string1 = string1 .. " " .. string9
	end
	
	if (string10 ~= nil) then
		string1 = string1 .. " " .. string10
	end
	
	if (string11 ~= nil) then
		string1 = string1 .. " " .. string11
	end
	
	if (string12 ~= nil) then
		string1 = string1 .. " " .. string12
	end
	
	if (string13 ~= nil) then
		string1 = string1 .. " " .. string13
	end
	
	if (string14 ~= nil) then
		string1 = string1 .. " " .. string14
	end
	
	if (string15 ~= nil) then
		string1 = string1 .. " " .. string15
	end
	
	if (string16 ~= nil) then
		string1 = string1 .. " " .. string16
	end
	
	if (string17 ~= nil) then
		string1 = string1 .. " " .. string17
	end
	
	if (string18 ~= nil) then
		string1 = string1 .. " " .. string18
	end
	
	if (string19 ~= nil) then
		string1 = string1 .. " " .. string19
	end
	
	if (string20 ~= nil) then
		string1 = string1 .. " " .. string20
	end
	
	if (string21 ~= nil) then
		string1 = string1 .. " " .. string21
	end
	
	if (string22 ~= nil) then
		string1 = string1 .. " " .. string22
	end
	
	if (string23 ~= nil) then
		string1 = string1 .. " " .. string23
	end
	
	if (string24 ~= nil) then
		string1 = string1 .. " " .. string24
	end
	
	if (string25 ~= nil) then
		string1 = string1 .. " " .. string25
	end
	
	if (string26 ~= nil) then
		string1 = string1 .. " " .. string26
	end
	
	if (string27 ~= nil) then
		string1 = string1 .. " " .. string27
	end
	
	if (string28 ~= nil) then
		string1 = string1 .. " " .. string28
	end
	
	if (string29 ~= nil) then
		string1 = string1 .. " " .. string29
	end
	
	if (string30 ~= nil) then
		string1 = string1 .. " " .. string30
	end
	
	if (string31 ~= nil) then
		string1 = string1 .. " " .. string31
	end
	
	if (string32 ~= nil) then
		string1 = string1 .. " " .. string32
	end
	
    player:serverMsg(string1)
	
end;
