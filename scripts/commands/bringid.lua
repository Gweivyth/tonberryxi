---------------------------------------------------------------------------------------------------
-- func: bring <player>
-- desc: Brings the target to the player.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "i"
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!bring <player> {forceZone}");
end;

function onTrigger(player, target, forceZone)
    
    local targ = GetMobByID( target )
    
    targ:setPos( player:getXPos(), player:getYPos(), player:getZPos(), player:getRotPos() )
	
    end
end