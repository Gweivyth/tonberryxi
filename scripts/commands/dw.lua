---------------------------------------------------------------------------------------------------
-- func: changejob
-- desc: Changes the players current job.
---------------------------------------------------------------------------------------------------

require("scripts/globals/status");

cmdprops =
{
    permission = 0,
    parameters = ""
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!flip");
end;

function onTrigger(player)
    ret = player:flip(18);
	
	if (ret == 0) then
		error(player, "... an error occurred.");
	end
end
