---------------------------------------------------------------------------------------------------
-- func: getid
-- desc: Prints the ID of the currently selected target under the cursor
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = ""
};

function onTrigger(player)
    local targ = player:getCursorTarget();
	
    if (targ ~= nil) then
        local gid = targ:getGroupID()
		
		if (gid == nil) then
			player:PrintToPlayer("!getgroupid ... an error occurred.");
			return 0
		end
		
		player:PrintToPlayer(string.format("%s's GROUP ID is: %u ", targ:getName(),gid));
    else
        player:PrintToPlayer("Must select a target using in game cursor first.");
    end
end;
