---------------------------------------------------------------------------------------------------
-- func: getid
-- desc: Prints the ID of the currently selected target under the cursor
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = ""
};

function onTrigger(player)

    local targ = player:getCursorTarget();
	
	if (targ == nil) then
        player:PrintToPlayer("Must select a target using in game cursor first.");
    end
	
	local eva = targ:getEVA();
    
    player:PrintToPlayer(string.format("%s's EVA is: %u ", targ:getName(),eva));

end;
