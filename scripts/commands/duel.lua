---------------------------------------------------------------------------------------------------
-- func:
-- desc:
---------------------------------------------------------------------------------------------------

require("scripts/globals/status");

cmdprops =
{
    permission = 2,
    parameters = "si"
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!duel");
end;

function onTrigger(player,team,sync)
    
	local teamno = 0
	
	if team == "green" then
		teamno = 1
	elseif team == "blue" then
		teamno = 2
	else
		teamno = 3 -- ffa
	end
	
	if sync == nil then
		sync = 0
	end
	
	player:setPVP(teamno,sync)
	
end
