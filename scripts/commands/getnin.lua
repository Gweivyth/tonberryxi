---------------------------------------------------------------------------------------------------
-- func: getid
-- desc: Prints the ID of the currently selected target under the cursor
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = ""
};

function onTrigger(player)
    local targ = player:getCursorTarget();
	local val = 10;
	if targ:getNinDebuff() ~= nil then
		val = targ:getNinDebuff();
    end
	if (targ ~= nil) then
        player:PrintToPlayer(string.format("%s's current ninjutsu weakness is: %u", targ:getName(),val));
    else
        player:PrintToPlayer("Must select a target using in game cursor first.");
    end
end;
