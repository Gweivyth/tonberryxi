---------------------------------------------------------------------------------------------------
-- func: godmode
-- desc: Toggles god mode on the player, granting them several special abilities.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = ""
};

function onTrigger(player)
    if (player:getVar("sGodMode") == 0) then
        -- Toggle GodMode on..
        player:setVar("sGodMode", 1);

        -- Add bonus effects to the player..


        player:addStatusEffect(dsp.effect.CHAINSPELL,1,0,0);

        player:addStatusEffect(dsp.effect.REFRESH,255,0,0);
        player:addStatusEffect(dsp.effect.REGEN,255,0,0);

        -- Heal the player from the new buffs..
        player:addHP( 50000 );
        player:setMP( 50000 );
    else
        -- Toggle GodMode off..
        player:setVar("sGodMode", 0);

        -- Remove bonus effects..

		
        player:delStatusEffect(dsp.effect.CHAINSPELL);

        player:delStatusEffect(dsp.effect.REFRESH);
        player:delStatusEffect(dsp.effect.REGEN);
    end
end